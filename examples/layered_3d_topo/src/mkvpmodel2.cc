#include<valarray>
extern "C" {
  #include<rsf.h>
}

#define I2D(i,j)   (i)*ny + (j)
#define I3D(i,j,k) (i)*ny*nz + (j)*nz + (k)

using Array = std::valarray<float>; //floating point STL Array

void mkvol(size_t nx, 
           size_t ny, 
           size_t nz, 
           float  dh, 
           Array &data,
     const Array &topo); 

int main(int argc, char* argv[]) {
  
  //init rsf environment
  sf_init(argc,argv);

  //rsf-data (in/out) environments
  sf_file Fin = sf_input("in");
  sf_file Fto = sf_input("topo");
  sf_file Fou = sf_output("out");

  size_t nz,ny,nx;
  float dh;

  //read axis parameters
  sf_axis axis_z = sf_iaxa(Fin,1); //first axis value
  sf_axis axis_y = sf_iaxa(Fin,2); //second axis value
  sf_axis axis_x = sf_iaxa(Fin,3); //third axis value

  //parameters dimension
  dh = sf_d(axis_z);
  nz = sf_n(axis_z);
  ny = sf_n(axis_y); 
  nx = sf_n(axis_x); 
 
  //size array data and memory allocation
  Array dat(nx*ny*nz);
  Array top(nx*ny);

  //read rsf-files inputs
  sf_floatread ( &dat[0],dat.size(),Fin ); //read 3D data
  sf_floatread ( &top[0],top.size(),Fto );  //read 2D data

	//initial three layers vp model
	for (size_t i=0; i<nx; i++) {
		for (size_t j=0; j<ny; j++) {
			for (size_t k=0;      k<nz/3;   k++) dat[I3D(i,j,k)] = 2.1;
			for (size_t k=nz/3;   k<2*nz/3; k++) dat[I3D(i,j,k)] = 3.0;
			for (size_t k=2*nz/3; k<nz;     k++) dat[I3D(i,j,k)] = 3.6;
		}
	}

  mkvol(nx,ny,nz,dh,dat,top);

  sf_floatwrite( &dat[0],dat.size(),Fou);

  //close rsf-data environment
  sf_close();

}

void mkvol(size_t nx, 
           size_t ny, 
           size_t nz, 
           float  dh, 
           Array &data,
           const Array &topo)
{
  size_t i,j,k;
  float h,elev;
  for ( i=0; i<nx; i++) {
    for ( j=0; j<ny; j++) {
      //read the elevation (+) to botton
      elev = nz*dh - topo[I2D(i,j)];
      for ( k=0; k<nz; k++) {
        h = k*dh;
        if( elev > h )
          data[ I3D(i,j,k) ] = 0.0;
      }
    }
  }
}
