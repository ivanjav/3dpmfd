from rsf.proj import *
from rsf.prog import RSFROOT
from math import sqrt, ceil, pi

# nz,ny,nx son las dimensiones del modelo
# dh es el paso de malla [km]
# amp es la amplitud de la variación
# ref es el nivel de referencia (datum)
partopo = dict(nz=300,ny=301,nx=201,dh=0.01,amp=-0.1,ref=0.9)

############### define la topografia en el plano YX ###################
# se define una funcion sinusoidad en 2D de amplitud de 0.1 km (sfmath) y 
# variación aleatoria (sfnoice) con un operador removedor de spikes 2D de 
# tamaño 19 (sfdespike2) y la aplicacion de un filtro para suavisar (sfsmooth) 
Flow('topox',None,'''math n1=%(ny)d n2=%(nx)d d1=%(dh)g d2=%(dh)g 
                     output="%(amp)g*sin(x1)*cos(x2)+%(ref)g" 
                     o1=0 o2=0 | put label1=Y label2=X title=model 
                     unit1=Km unit2=Km barunit="Km" barlabel="Topography"  
                     | noise seed=123456 | despike2  wide1=19 wide2=19 
                     | sfsmooth repeat=1 rect1=20 rect2=20 
                   '''%(partopo))  

# remapea aumentando el doble de celdas
Flow('topog','topox',''' remap1 n1=601 d1=0.005 | transp plane=12 | 
		                     remap1 n1=401 d1=0.005 | transp plane=12 | 
												 math  output="2.1-input" ''')

############### visualización 2D de la topografia #####################
ratio = 0.9*partopo['ny']/partopo['nx']
Result('topog','''math output="input" | transp plane=12 | 
		              grey color=j mean=y scalebar=y screenRatio=%g'''%(ratio))


# make a dummy initial 3d vpmodel
par = dict(nz=300,ny=601,nx=401,dh=0.005)
Flow('dummy',None,'''sfspike mag=1 n1=%(nz)d n2=%(ny)d n3=%(nx)d  
                     d1=%(dh)g d2=%(dh)g d3=%(dh)g o1=0 o2=0 o3=0 
                     label1="Z" label2="Y" label3="X" unit1="Km" 
                     unit2="Km" unit3="Km" '''%(par))

# programa que construye el modelo de velocidad con la topografia
# y genera basado en el modelo vp la densidad rho y velocidad-s vs
proj = Project()
exa = proj.Program(['src/mkvpmodel2.cc'])
exb = proj.Program(['src/vp2rho.cc'])
exc = proj.Program(['src/vp2vs.cc'])

#build velocity vp model with Topography
Flow('vp',['dummy','topog',str(exa[0])],'''./${SOURCES[2]} topo=${SOURCES[1]} 
                                          | sfput title="vp-model" barlabel="velocity" barunit="km/s"  ''')

#build rho model based on Brocher T. M. (2005). Empirical relations 
#between elastic wavespeeds and density in the earth's crust Bulletin 
#of the Seismological Society of America 95, 2081-2092.
Flow('rho',['vp',str(exb[0])],'''./${SOURCES[1]}  | sfput title="rho-model" 
                                 barlabel="density" barunit="gr/cc"''')

#build vs model based on Brocher T. M. (2005). Empirical relations 
#between elastic wavespeeds and density in the earth's crust Bulletin 
#of the Seismological Society of America 95, 2081-2092.
Flow('vs', ['vp',str(exc[0])],'''./${SOURCES[1]}  | sfput title="vs-model" 
		                             barlabel="velocity" barunit="km/s" ''')

ratio2 = 0.9*par['nz']/par['ny']

Result('vp','grey color=j mean=y scalebar=y barreverse=y screenRatio=%g'%ratio2)
Result('vs','grey color=j mean=y scalebar=y barreverse=y screenRatio=%g'%ratio2)
Result('rho','grey color=j mean=y scalebar=y barreverse=y screenRatio=%g'%ratio2)


# Moleding parameters
par.update(dict(Vmax=3.6, time=1.5, Ts=0.004, f=10, pi=pi, Ts_field=4.0, nb=40))
dt = 0.98/(par['Vmax']*(0.0+1./24+9./8)*sqrt(3./(par['dh']**2)))
skip_ts = ceil(par['Ts']/dt)
par['dt'] = par['Ts']/skip_ts
par['tout'] = par['time']//par['dt']+par['Ts']//par['dt']

# Source wavelet (Ricker)
Flow('wavelet',None,'''sfmath n1=%(tout)d d1=%(dt)g
                       output="(1-2*(%(pi)g*%(f)g)^2*(x1-1.2/%(f)g)^2)*exp(-(%(pi)g*%(f)g)^2*(x1-1.2/%(f)g)^2)"
                    '''%par)

########################################
#parametros de geometria de adqusicion
########################################
orx = 0.0  #origen receptores en X
ory = 1.1  #origen receptores en Y
drx = 0.01   #intervalo entre receptoras en X [m]
dry = 0.05   #intervalo entre receptoras en Y [m]
nrx = 301  #numero receptores en X
nry = 1  #numero receptores en Y
osx = 1.5  #origen fuente en X
osy = 1.  #origen fuente en Y
dsx = 2.  #intervalo entre disparos en X [m]
dsy = 2.  #intervalo entre disparos en Y [m]
nsx = 1    #numero fuentes en X
nsy = 1    #numero fuentes en Y
rd = 0.01   # receivers depth
sd = 0.02   # sources depth
nr = nrx*nry # numero total de receptores
ns = nsx*nsy # numero total de fuentes

print ('*****************************')
print ('* Parametros de modelado    *')
print ('*****************************')
print ('   dt = %(dt)g [s]' % (par) )
print ('   dh = %(dh)g [m]' % (par) )
print ('   Fq = %(f)g [Hz]' % (par) )
print ('   Time = %g [seg]' % (par['dt']*par['tout']) )
print ('                             ')
print ('*****************************')
print ('* Parametros de adquisicion *')
print ('*****************************')
print ('   drx = %g [m]' % (drx) )
print ('   dsx = %g [m]' % (dsx) )
print ('   dry = %g [m]' % (dry) )
print ('   dsy = %g [m]' % (dsy) )
print ('   ng = %d ' % (nr) )
print ('   ns = %d ' % (ns) )
print ('*****************************' )

# X-Coordinate sources
Flow('Sx',None,'''sfmath n1=%d d1=%g o1=%g n2=%d output="x1" | put n1=%d n2=1
               '''%(nsx,dsx,osx,nsy,ns))

# Y-Coordinate sources
Flow('Sy',None,'''sfmath n2=%d d2=%g o2=%g n1=%d output="x2" | put n1=%d n2=1
               '''%(nsy,dsy,osy,nsx,ns))

# Z-Coordinate sources
Flow('Sz',None,'''sfspike n1=%d mag=%g
               '''%(ns,sd))

# X-Coordinate sources
Flow('Rx',None,'''sfmath n1=%d d1=%g o1=%g n2=%d output="x1" | put n1=%d n2=1
               '''%(nrx,drx,orx,nry,nr))
# Y-Coordinate receivers
Flow('Ry',None,'''sfmath n2=%d d2=%g o2=%g n1=%d output="x2" | put n1=%d n2=1
               '''%(nry,dry,ory,nrx,nr))

# Z-Coordinate receivers on topography
Flow('Rz',None,'''sfspike n1=%d mag=%g
               '''%(nr,rd))

# Together coordinate sources
Flow('sources',['Sy','Sx','Sz'],'sfcat ${SOURCES[1:3]} axis=2 | sftransp')
Flow('receivers',['Ry','Rx','Rz'],'''sfcat ${SOURCES[1:3]} axis=2 | sftransp''')


# 3D elastic wave modeling
#codepath="../../programs/sfpmfd3d_cpu"
codepath="../../programs/sfpmfd3d_gpu"


Flow(['field','traceX','traceY','traceZ','top'],['wavelet','vp','vs','rho','sources','receivers'],
                                                               '''%s
                                                                   --input=${SOURCES[0]}
                                                                   vp=${SOURCES[1]}
                                                                   vs=${SOURCES[2]}
                                                                   rho=${SOURCES[3]}
                                                                   sou=${SOURCES[4]}
                                                                   rec=${SOURCES[5]}
                                                                   Ts=%g
                                                                   Fq=%g
                                                                   Vmax=%g
                                                                   Ts_field=%g      
                                                                   nb=%d                                                        
                                                                   gatherX=${TARGETS[1]}
                                                                   gatherY=${TARGETS[2]}
                                                                   gatherZ=${TARGETS[3]}
                                                                   top=${TARGETS[4]}
                                                                   --output=${TARGETS[0]}
                                                                '''%(codepath,par['Ts'],par['f'],par['Vmax'],par['Ts_field'],par['nb']))



# Figure topography
Result('top','top','''transp plane=12 | grey color=j mean=y title="topography" yreverse=n scalebar=y ''')

# Figure shot gather
Flow('traceX2','traceX','''put o2=%g d2=%g n2=%d o3=%g d3=%g n3=%d o4=%g d4=%g n4=%d o5=%g d5=%g n5=%d 
                        '''%(orx,drx,nrx,ory,dry,nry,osx,dsx,nsx,osy,dsy,nsy))
Flow('traceY2','traceY','''put o2=%g d2=%g n2=%d o3=%g d3=%g n3=%d o4=%g d4=%g n4=%d o5=%g d5=%g n5=%d 
                        '''%(orx,drx,nrx,ory,dry,nry,osx,dsx,nsx,osy,dsy,nsy))
Flow('traceZ2','traceZ','''put o2=%g d2=%g n2=%d o3=%g d3=%g n3=%d o4=%g d4=%g n4=%d o5=%g d5=%g n5=%d 
                        '''%(orx,drx,nrx,ory,dry,nry,osx,dsx,nsx,osy,dsy,nsy))
                    
clip=1e-5

cases = ["Z","X","Y"]
for tag in cases:
   Result('trace'+tag,'trace'+tag+'2','''window f4=0 n4=1 |
                              grey color=i clip=%g
                              title="%s-Component" scalebar=y
                              label1='Time' label2='Trace number' unit1=s
                           '''%(clip,tag))


End()
