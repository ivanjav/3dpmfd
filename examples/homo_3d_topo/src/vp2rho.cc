#include<valarray>
extern "C" {
  #include<rsf.h>
}

using Array = std::valarray<float>; //floating point STL Array

void vp2rho ( Array &out, const Array &vel );
int main(int argc, char *argv[]) {

  size_t nx,ny,nz;

  sf_init(argc,argv);

  sf_file Fin = sf_input("in");
  sf_file Fou = sf_output("out");

  sf_axis axis_z = sf_iaxa(Fin,1); //first axis
  sf_axis axis_y = sf_iaxa(Fin,2); //second axis
  sf_axis axis_x = sf_iaxa(Fin,3); //third axis

  nz = sf_n(axis_z);
  ny = sf_n(axis_y);
  nx = sf_n(axis_x);

  Array vel(nx*ny*nz);
  Array rho(nx*ny*nz);
  
  sf_floatread ( &vel[0],vel.size(),Fin ); //vp [Km/s]

  vp2rho(rho,vel);//Brocher T. M. (2005)

  sf_floatwrite( &rho[0],rho.size(),Fou);

}

//Brocher T. M. (2005). Empirical relations between 
//elastic wavespeeds and density in the earth's crust.
//Bulletin of the Seismological Society of America 95, 2081-2092.
//only valid for values between 1.5 < Vp[Km/s] < 8.0
void vp2rho ( Array &out, const Array &vel ) {
  size_t i;
  float const k = 0.000106,l =-0.0043, m = 0.0671,
              n =-0.4721, o = 1.6612;
  for (i=0; i<vel.size(); i++){ //rho [gr/cc]
    if (vel[i] > 1.5 && vel[i] < 8.0)
      out[i] = ((((k*vel[i]+l)*vel[i]+m)*vel[i]+n)*vel[i]+o)*vel[i];
    else
      out[i] = 0.0;
  }
}
