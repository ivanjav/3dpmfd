#include<valarray>
extern "C" {
  #include<rsf.h>
}

using Array = std::valarray<float>; //floating point STL Array

void vp2vs (Array &out, const Array &vel );

int main(int argc, char *argv[]) {

  size_t nx,ny,nz;

  sf_init(argc,argv);

  sf_file Fin = sf_input("in");
  sf_file Fou = sf_output("out");

  sf_axis axis_z = sf_iaxa(Fin,1); //first axis
  sf_axis axis_y = sf_iaxa(Fin,2); //second axis
  sf_axis axis_x = sf_iaxa(Fin,3); //third axis

  nz = sf_n(axis_z);
  ny = sf_n(axis_y);
  nx = sf_n(axis_x);

  Array vel(nx*ny*nz);
  Array  vs(nx*ny*nz);

  sf_floatread ( &vel[0],vel.size(),Fin ); //vp [Km/s]

  vp2vs(vs,vel); //Brocher T. M. (2005)

  sf_floatwrite( &vs[0],vs.size(),Fou );

  sf_close();

}
//Brocher T. M. (2005). Empirical relations between 
//elastic wavespeeds and density in the earth's crust.
//Bulletin of the Seismological Society of America 95, 2081-2092.
//only valid for values between 1.5 < Vp[Km/s] < 8.0
void vp2vs ( Array &out, const Array &vel ) {
  float const a= 0.0064,b=-0.1238,c= 0.7949,
              d=-1.2344,e= 0.7858;
  for (size_t i=0; i<vel.size(); i++){ //vs [Km/s]
    if (vel[i] > 1.5 && vel[i]< 8.0)
      out[i] = (((a*vel[i]+b)*vel[i]+c)*vel[i]+d)*vel[i]+e;
    else
      out[i] = 0.0;
  }
}
