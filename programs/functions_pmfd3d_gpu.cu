/*
 ============================================================================
 Name        : functions_pmfd3d_gpu.cu
 Author      : Ivan Sánchez
 Version     :
 Copyright   :
 Description : Functions host.
 ============================================================================
 */

#define Id(A,i,j,k) (A)[ (i)*ny*nz + (j)*nz + (k) ]

#define X(a,i) (a)[3*(i)]
#define Y(a,i) (a)[3*(i)+1]
#define Z(a,i) (a)[3*(i)+2]

#define STENCIL_WIDTH 2
/*   FD Coefficients */
#define C1 9.0f/8.0f
#define C2 -1.0f/24.0f
/* backward  FD derivative stencils */
#define Dxb(A,i,j,k,s) (C2*( (A)[(i+1)*ny*nz+(j)*nz+(k)] - (A)[(i-2)*ny*nz+(j)*nz+(k)] ) +	\
  	     C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i-1)*ny*nz+(j)*nz+(k)] ) )*s
#define Dyb(A,i,j,k,s) (C2*( (A)[(i)*ny*nz+(j+1)*nz+(k)] - (A)[(i)*ny*nz+(j-2)*nz+(k)] ) +	\
  		C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i)*ny*nz+(j-1)*nz+(k)] ) )*s
#define Dzb(A,i,j,k,s) (C2*( (A)[(i)*ny*nz+(j)*nz+(k+1)] - (A)[(i)*ny*nz+(j)*nz+(k-2)] ) +	\
  		C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i)*ny*nz+(j)*nz+(k-1)] ) )*s

/* forward FD derivative stencils */
#define Dxf(A,i,j,k,s) (C2*((A)[(i+2)*ny*nz+(j)*nz+(k)] - (A)[(i-1)*ny*nz+(j)*nz+(k)] )+	\
  		C1*((A)[(i+1)*ny*nz+(j)*nz+(k)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s
#define Dyf(A,i,j,k,s) (C2*((A)[(i)*ny*nz+(j+2)*nz+(k)] - (A)[(i)*ny*nz+(j-1)*nz+(k)] ) +	\
  		C1*((A)[(i)*ny*nz+(j+1)*nz+(k)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s
#define Dzf(A,i,j,k,s) (C2*((A)[(i)*ny*nz+(j)*nz+(k+2)] - (A)[(i)*ny*nz+(j)*nz+(k-1)] ) +	\
  		C1*((A)[(i)*ny*nz+(j)*nz+(k+1)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s

/* Funcion de tiempo*/
double time_measure(){
	struct timeval tp;
	gettimeofday(&tp,NULL);
	return ((double)tp.tv_sec +(double)tp.tv_usec*1.e-6);
}
/*******************************************/
/* Expand model*/
/*******************************************/

void expand(float*vv, float *v0, int nz, int nx, int ny, int nz1, int nx1, int ny1)
{
    int i,j,k,i1,j1,k1,nb;
    nb = (nz - nz1);
    for(i=0; i<nx; i++){
	    for(j=0; j<ny; j++){
            for(k=0; k<nz; k++)
	        {
                k1=(k<nz1)?k:(nz1-1);
                i1=(i<nb)?0:((i>=(nx1+nb))?(nx1-1):i-nb);
                j1=(j<nb)?0:((j>=(ny1+nb))?(ny1-1):j-nb);
                Id(vv,i,j,k) = v0[(i1)*ny1*nz1+(j1)*nz1+(k1)];
	        }
        }
    }
}

/*******************************************/
/* Window model*/
/*******************************************/


void window(float*v0, float *vv, int nz1, int nx1, int ny1, int nz, int nx, int ny)
{
    int i,j,k,i1,j1,k1,nb;
    nb = (nz - nz1);
    for(i1=0; i1<nx1; i1++){
	    for(j1=0; j1<ny1; j1++){
            for(k1=0; k1<nz1; k1++)
	        {
                k=k1;
                i=i1+nb; 
                j=j1+nb; 
                v0[(i1)*ny1*nz1+(j1)*nz1+(k1)] = Id(vv,i,j,k);
	        }
        }
    }
}


/*******************************************/
/* Check receivers and sources position*/
/*******************************************/
bool check_pos(float *s, int ns, float ox, float oy, float oz, float dx, float dy, float dz, int nx, int ny, int nz){
    int i;
    for (i=0; i<ns; i++){
        if ((X(s,i) <  ox) || (X(s,i) >  ox+dx*(nx-1)) ) return true; 
        if ((Y(s,i) <  oy) || (Y(s,i) >  oy+dy*(ny-1)) ) return true;  
        if ((Z(s,i) <  oz) || (Z(s,i) >  oz+dz*(nz-1)) ) return true;  
    } 
    return false;
}

/*******************************************/
/* Update position*/
/*******************************************/
void update_pos(float *s, int ns, int nb, float dx,  float dy){
    int i;
    for (i=0; i<ns; i++){
        X(s,i) +=  dx*nb ;
        Y(s,i) +=  dy*nb ;
    } 
}


/*******************************************/
/* Compute ABC*/
/*******************************************/

void init_abc(int bound,
              float vmax,
              float R,
              float dt,
              float dh,
              float *a)
{
    float L=bound*dh;
    float d0 = -4*vmax*logf(R)/L;
    float sigma;
    for(int i=0; i<bound; i++){
        //sigma = d0*pow((bound-i)*dh/L,4);
        //a[i] = exp(-sigma*dt);
        sigma = pow(0.01*(bound-i),2);
        a[i] = exp(-sigma);
    }
}


/*******************************************/
/* Compute PML coefficients */
/*******************************************/

void init_pml(float *coef, 
                int nb, 
                float vmax, 
                float dt, 
                float dh,
                int ny, 
                int nz)
{
    float L=(nb-1)*dh;
    float R = 1e-6;
    float d0 = -3*(vmax)*log(R)/(2*L);
    float x;
    for(int i=0; i<nb; i++){
        x=L-i*dh;
        coef[i]=exp(-d0*dt*pow(x / L, 2));
    }


}


/*******************************************/
/* Compute topography*/
/*******************************************/
void topography (float *vs, 
                 int *top, 
                 int nx, 
                 int ny, 
                 int nz){
     int i, j, k;
     bool H, HT, VRT, VLT, VFT, VBT, OR, OL, OF, OB, IR, IB;
     for (i=1; i<nx-1; i++){
      	 for (j=1; j<ny-1; j++){
            for (k=1; k<nz-1; k++){
                // if (Id(vs,i,j,k)!=0 &&   Id(vs,i,j,k-1) ==0    && ( ( Id(vs,i-1,j,k)+Id(vs,i+1,j,k)==0 )   ||  ( Id(vs,i,j-1,k)+Id(vs,i,j+1,k)==0 ) ) )
                //     Id(vs,i,j,k)=0;
                HT  = ( Id(vs,i,j,k)!=0 && Id(vs,i,j,k-1) ==0 );
                VRT = ( Id(vs,i,j,k)!=0 && Id(vs,i+1,j,k) ==0 );
                VLT = ( Id(vs,i,j,k)!=0 && Id(vs,i-1,j,k) ==0 );
                VFT = ( Id(vs,i,j,k)!=0 && Id(vs,i,j-1,k) ==0 );
                VBT = ( Id(vs,i,j,k)!=0 && Id(vs,i,j+1,k) ==0 );
                OR = ( HT && VRT );
                OL = ( HT && VLT );
                OB = ( HT && VBT );
                OF = ( HT && VFT );
                IR = ( Id(vs,i+1,j,k-1)==0 && !HT && !VRT);
                IB = ( Id(vs,i,j+1,k-1)==0 && !HT && !VBT);
                H  = ( HT && !OR && !OL && !OF && !OB );
                if ( H || OL || OF || IR || IB ) {top[(i)*ny+j]=k;}
            }
         }
     }

    for (i=0; i<nx; i++)
        top[(i)*ny]=top[(i)*ny+1];
    for (j=0; j<ny; j++)
        top[j]=top[ny+j];
    for (j=0; j<ny; j++)
        top[(nx-1)*ny+j]=top[(nx-2)*ny+j];
}

int top_max (float *rho, 
             int nx, 
             int ny, 
             int nz){
     int i, j, k, nz_ns;
     nz_ns=0;
     for (i=0; i<nx; i++){
      	 for (j=0; j<ny; j++){
             for (k=1; k<nz; k++){
                if (rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz+(j)*nz+(k-1)] ==0 && nz_ns < k+1) {nz_ns=k+1;}
            }
         }
     }
     return nz_ns;
}


/*******************************************/
/* Compute number of points in surface
   and interior regions*/
/*******************************************/
void indices_lengths(float *rho, 
                     int nx, 
                     int ny, 
                     int nz, 
                     int *cont_s, 
                     int *cont_i){
    int i, j, k;
		*cont_s=0;
		*cont_i=0;
		bool H, VR, VL, VF, VB, TP;

        for (i=2; i<nx-2; i++){
            for (j=2; j<ny-2; j++){
                for (k=2; k<nz-2; k++){
    				H  = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)] ==0 );
    				VR = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i+1)*ny*nz + (j)*nz + (k)] ==0 );
    				VL = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i-1)*ny*nz + (j)*nz + (k)] ==0 );
    				VF = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j-1)*nz + (k)] ==0 );
    				VB = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j+1)*nz + (k)] ==0 );
    				TP = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)]*rho[(i-1)*ny*nz + (j)*nz + (k-1)]*rho[(i+1)*ny*nz + (j)*nz + (k-1)]*rho[(i)*ny*nz + (j-1)*nz + (k-1)]*rho[(i)*ny*nz + (j+1)*nz + (k-1)] ==0 );
                    if ( H || VR || VL || VF || VB || TP) {
                        *cont_s+=1;
                    }
    				if ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && !( H || VR || VL || VF || VB || TP )) {
                        *cont_i+=1;
                    }
                }
            }
        }
}

/*******************************************/
/* Compute indices arrays for surface
   and interior regions*/
/*******************************************/
void indices_arrays(float *rho, 
                    int nx, 
                    int ny, 
                    int nz, 
                    int *ind_s, 
                    int *ind_i){
    int i, j, k;
		int cont_s=0;
		int cont_i=0;
		bool H, VR, VL, VF, VB, TP;
        for (i=2; i<nx-2; i++){
          	for (j=2; j<ny-2; j++){
                 for (k=2; k<nz-2; k++){
                    H  = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)] ==0 );
                    VR = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i+1)*ny*nz + (j)*nz + (k)] ==0 );
                    VL = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i-1)*ny*nz + (j)*nz + (k)] ==0 );
                    VF = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j-1)*nz + (k)] ==0 );
                    VB = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j+1)*nz + (k)] ==0 );
                    TP = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)]*rho[(i-1)*ny*nz + (j)*nz + (k-1)]*rho[(i+1)*ny*nz + (j)*nz + (k-1)]*rho[(i)*ny*nz + (j-1)*nz + (k-1)]*rho[(i)*ny*nz + (j+1)*nz + (k-1)] ==0 );

                    if ( H || VR || VL || VF || VB || TP) {
                        ind_s[cont_s]= (i)*ny*nz + (j)*nz + (k);
                        cont_s+=1;
                    }
                    if ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && !( H || VR || VL || VF || VB || TP )) {
                        ind_i[cont_i]= (i)*ny*nz + (j)*nz + (k);
                        cont_i+=1;
                    }
                }
            }
        }
}

/*******************************************/
/* Compute indices arrays for receivers*/
/*******************************************/

void indices_length_rec(int *cont_g, 
                        float *rece, 
                        int ng, 
                        int nx, 
                        float idx){
    int g;
    int Gx;
    *cont_g=0;
    for (g=0; g<ng; g++){
        Gx= (int) (X(rece,g)*idx);
        if (Gx >= STENCIL_WIDTH && Gx < nx-STENCIL_WIDTH){
            *cont_g+=1;
        }
    }
}

void indices_array_rec(int *ind_g, 
                      float *rece, 
                      int *top, 
                      int ng, 
                      int nx, 
                      float idx){
    int g;
    int Gx;
    int cont_g=0;

    for (g=0; g<ng; g++){
        Gx= (int) (X(rece,g)*idx);
        if (Gx >= STENCIL_WIDTH && Gx < nx-STENCIL_WIDTH){
            ind_g[cont_g]=g;
            cont_g+=1;
        }
    }
}


/*******************************************/
/* Compute parameters in the surface region*/
/*******************************************/
void parameter_fs(float *velp,
                  float *vels,
                  float *rho,
                  float *bxs,
                  float *bys,
                  float *bzs,
                  float *mu_xys,
                  float *mu_yzs,
                  float *mu_zxs,
                  float *n1_xxs,
                  float *n2_xxs,
                  float *n3_xxs,
                  float *n1_yys,
                  float *n2_yys,
                  float *n3_yys,
                  float *n1_zzs,
                  float *n2_zzs,
                  float *n3_zzs,
                  int *ind_s,
                  int cont_s,
                  int nx,
                  int ny,
                  int nz
                  )
{
     int i, j, k, ind;
     float b, b_i, b_j, b_k, bx, by, bz;
     float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_jk, mu_xy, mu_yz, mu_zx;
     float n1_xx, n2_xx, n3_xx, n1_yy, n2_yy, n3_yy, n1_zz, n2_zz, n3_zz;
     bool H, VR, VL, VF, VB, TP, IBR, IB, IR, OP;// IL, IF;

     for (ind=0; ind<cont_s; ind++){
          i= ind_s[ind]/(ny*nz);
		j= (ind_s[ind]-(i)*(ny*nz))/nz;
      	k= ind_s[ind]- ((i)*ny*nz + (j)*nz);
          mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
          lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;
          if (Id(rho,i,j,k)!=0){b = 1/Id(rho,i,j,k);}  else{b = 0;}
          if (Id(rho,i+1,j,k)!=0){ b_i= 1/Id(rho,i+1,j,k); bx=2*b*b_i/(b+b_i);}  else{bx =0;}
          if (Id(rho,i,j+1,k)!=0){b_j= 1/Id(rho,i,j+1,k);  by=2*b*b_j/(b+b_j);}  else{by = 0;}
          if (Id(rho,i,j,k+1)!=0){b_k= 1/Id(rho,i,j,k+1);  bz=2*b*b_k/(b+b_k);}  else{bz = 0;}

          mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k);
          mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k);
          mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1);
          mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k);
          mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1);
          mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1);

          if (mu!=0 && mu_i!=0 && mu_j!=0 && mu_ij!=0){ mu_xy = 1/(0.25/mu+0.25/mu_i+0.25/mu_j+0.25/mu_ij);}
          else{ mu_xy = 0;}
          if (mu!=0 && mu_j!=0 && mu_k!=0 && mu_jk!=0){ mu_yz = 1/(0.25/mu+0.25/mu_j+0.25/mu_k+0.25/mu_jk);}
          else{mu_yz = 0;}
          if (mu!=0 && mu_i!=0 && mu_k!=0 && mu_ik!=0){ mu_zx = 1/(0.25/mu+0.25/mu_i+0.25/mu_k+0.25/mu_ik);}
          else{ mu_zx = 0;}


          if (Id(rho,i,j,k)!=0 &&   Id(rho,i,j,k-1) ==0    && ( ( Id(rho,i-1,j,k)+Id(rho,i+1,j,k)==0 )   ||  ( Id(rho,i,j-1,k)+Id(rho,i,j+1,k)==0 ) ) ){
               Id(rho,i,j,k)=0;
               mu=0;
               lambda=0;
            //    mu_xy = 0;
            //    mu_yz = 0;
            //    mu_zx = 0;
          }

          n1_xx=lambda+2*mu;  n2_xx=lambda;         n3_xx=lambda;
          n1_yy=lambda;       n2_yy=lambda+2*mu;    n3_yy=lambda;
          n1_zz=lambda;       n2_zz=lambda;         n3_zz=lambda+2*mu;

          bxs[ind]=bx; bys[ind]=by; bzs[ind]=bz;
          mu_xys[ind]=mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
          n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
          n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
          n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;

          H  = ( Id(rho,i,j,k)!=0 && Id(rho,i,j,k-1) ==0 );
          VR = ( Id(rho,i,j,k)!=0 && Id(rho,i+1,j,k) ==0 );
          VL = ( Id(rho,i,j,k)!=0 && Id(rho,i-1,j,k) ==0 );
          VF = ( Id(rho,i,j,k)!=0 && Id(rho,i,j-1,k) ==0 );
          VB = ( Id(rho,i,j,k)!=0 && Id(rho,i,j+1,k) ==0 );
          TP = ( Id(rho,i,j,k)!=0 && Id(rho,i,j,k-1)*Id(rho,i-1,j,k-1)*Id(rho,i+1,j,k-1)*Id(rho,i,j-1,k-1)*Id(rho,i,j+1,k-1) ==0 );
          OP = ( (H && (VR || VL || VF || VB) ) || (VR && (VF || VB) ) || ( VL && (VF || VB) ) || (VL && VR) || (VF && VB) );
          IR = ( TP && !H && !OP && !VR && Id(rho,i+1,j,k-1)==0 );
          IB = ( TP && !H && !OP && !VB && Id(rho,i,j+1,k-1)==0 );
          IBR = ( IR && IB );
//                 IL = ( TP && !H && !OP && !VL && Id(rho,i-1,j,k-1)==0 );
//                 IF = ( TP && !H && !OP && !VF && Id(rho,i,j-1,k-1)==0 );

          if ( H && !OP ) {
               bxs[ind]=2*bx; bys[ind]=2*by; bzs[ind]=bz;
               mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
               n1_xxs[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n2_xxs[ind]=mu*lambda/(lambda+2*mu); n3_xxs[ind]=0;
               n1_yys[ind]=mu*lambda/(lambda+2*mu); n2_yys[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n3_yys[ind]=0;
               n1_zzs[ind]=0; n2_zzs[ind]=0; n3_zzs[ind]=0;
          }

          if ( VR && !( H || VL || VF || VB) ) {
               bxs[ind]=bx; bys[ind]=2*by; bzs[ind]=2*bz;
               mu_xys[ind]=mu_xy; mu_yzs[ind]=0.5*mu_yz; mu_zxs[ind]=mu_zx;
               n1_xxs[ind]=0; n2_xxs[ind]=0; n3_xxs[ind]=0;
             	n1_yys[ind]=0; n2_yys[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n3_yys[ind]=mu*lambda/(lambda+2*mu);
             	n1_zzs[ind]=0; n2_zzs[ind]=mu*lambda/(lambda+2*mu); n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

  	     if ( VL && !( H || VR || VF || VB) ) {
               bxs[ind]=bx; bys[ind]=2*by; bzs[ind]=2*bz;
             	mu_xys[ind]=mu_xy; mu_yzs[ind]=0.5*mu_yz; mu_zxs[ind]=mu_zx;
              	n1_xxs[ind]=0; n2_xxs[ind]=0; n3_xxs[ind]=0;
              	n1_yys[ind]=0; n2_yys[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n3_yys[ind]=mu*lambda/(lambda+2*mu);
        	     n1_zzs[ind]=0; n2_zzs[ind]=mu*lambda/(lambda+2*mu); n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

          if ( VF && !( H || VL || VR || VB) ) {
               bxs[ind]=2*bx; bys[ind]=by; bzs[ind]=2*bz;
              	mu_xys[ind]=mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=0.5*mu_zx;
              	n1_xxs[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n2_xxs[ind]=0; n3_xxs[ind]=mu*lambda/(lambda+2*mu);
              	n1_yys[ind]=0; n2_yys[ind]=0; n3_yys[ind]=0;
              	n1_zzs[ind]=mu*lambda/(lambda+2*mu); n2_zzs[ind]=0; n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

          if (  VB && !( H || VL || VR || VF) ) {
               bxs[ind]=2*bx; bys[ind]=by; bzs[ind]=2*bz;
              	mu_xys[ind]=mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=0.5*mu_zx;
              	n1_xxs[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n2_xxs[ind]=0; n3_xxs[ind]=mu*lambda/(lambda+2*mu);
              	n1_yys[ind]=0; n2_yys[ind]=0; n3_yys[ind]=0;
        	     n1_zzs[ind]=mu*lambda/(lambda+2*mu); n2_zzs[ind]=0; n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

          if ( OP ) {
               bxs[ind]=2*bx; bys[ind]=2*by; bzs[ind]=2*bz;
              	mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=0.5*mu_yz; mu_zxs[ind]=0.5*mu_zx;
        	     n1_xxs[ind]=0; n2_xxs[ind]=0; n3_xxs[ind]=0;
              	n1_yys[ind]=0; n2_yys[ind]=0; n3_yys[ind]=0;
              	n1_zzs[ind]=0; n2_zzs[ind]=0; n3_zzs[ind]=0;
          }

          if (  IBR ) {
               bxs[ind]=bx; bys[ind]=by; bzs[ind]=bz;
              	mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
              	n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
              	n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
        	     n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;
          }

        	if ( IR && !IBR ) {
               bxs[ind]=2*bx; bys[ind]=by; bzs[ind]=bz;
        	     mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
              	n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
        	     n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
        	     n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;
          }

        	if ( IB && !IBR ) {
               bxs[ind]=bx; bys[ind]=2*by; bzs[ind]=bz;
              	mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
        	     n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
              	n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
        	     n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;
          }

    }
}
