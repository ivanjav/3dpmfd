/*
 ============================================================================
 Name        : pmfd3d_cpu.cu
 Author      : Ivan Sánchez
 Version     :
 Copyright   :
 Description : Elastic Modeling including 3D Thread Layout, Sponge ABC and complete vx field transfer.
 ============================================================================
 */


extern "C" {
#include <rsf.h>
}
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <math.h>
#include <sys/time.h>
#include "functions_pmfd3d_gpu.cu"
#include "kernels_pmfd3d_gpu.cu"

#define X(a,i) (a)[3*(i)]
#define Y(a,i) (a)[3*(i)+1]
#define Z(a,i) (a)[3*(i)+2]

int main(int argc, char* argv[]){

   	int i,j;     // index variables
	int nt,nz,nx,ny,nz1,nx1,ny1; // dimensions
	int skip,skip_ts,ns,ng,nb;   // dimensions
	float dt,dz,dx,dy,idx,idy,idz,ox,oy,oz; // cells, & steptime sizes
	float Sxx,Szz,Syy,Vmax,Ts,Ts_field;    // source coordinates in meters
	bool pml;
  
	// Initialize RSF 
	sf_init(argc,argv);
		
	// setup I/O files 
	sf_file Fw  = sf_input ("--input");  //wavelet input
	sf_file Fvp = sf_input ("vp");  //p-velocity media
	sf_file Fvs = sf_input ("vs");  //s-velocity media
	sf_file Frho = sf_input ("rho"); //rho-density media
	sf_file Fso = sf_input ("sou"); //sources
	sf_file Fre = sf_input ("rec"); //receivers
	sf_file Ffo = sf_output("--output"); //wave field [Vz,Vy,Vx]
	sf_file FgZ = sf_output("gatherZ");   // gather Z-component
	sf_file FgY = sf_output("gatherX");   // gather Y-component
	sf_file FgX = sf_output("gatherY");   // gather X-component
	sf_file Ftop = sf_output("top");  // Topography
	
	// parameter from the command line
	if (!sf_getfloat("Ts_field",&Ts_field))
		sf_error("Need Ts_field="); // skip for field animation

	if (!sf_getfloat("Ts",&Ts))
		sf_error("Need Ts=");   //  time sampling

	if (!sf_getfloat("Vmax",&Vmax))
		sf_error("Need Vmax="); // maximum velocity

	if (!sf_getint("nb",&nb)) nb = 24 ;

	if (!sf_getbool("pml",&pml)) pml = 1;

	sf_warning("PML: %d\n", pml);
   // read time axis
	sf_axis axis_t  = sf_iaxa(Fw,1); //first value
		
	// read space axis	
	sf_axis axis_z = sf_iaxa(Fvp,1); //second value
	sf_axis axis_y = sf_iaxa(Fvp,2); //second value
	sf_axis axis_x = sf_iaxa(Fvp,3); //second value
	
	// Read sources and receivers axes parameters (n2,d2,o2)
	sf_axis axis_sou = sf_iaxa(Fso,2);
	sf_axis axis_rec = sf_iaxa(Fre,2);

	// read data information
	nt = sf_n(axis_t); //wavelet number data
    dt = sf_d(axis_t); //wavelet step size
	nz1 = sf_n(axis_z); //number dimension
    ny1 = sf_n(axis_y); //number dimension
    nx1 = sf_n(axis_x); //number dimension
	dz = sf_d(axis_z); //grid size dimension
	dy = sf_d(axis_y); //grid size dimension
	dx = sf_d(axis_x); //grid size dimension
	oz = sf_o(axis_z); //origin dimension
	oy = sf_o(axis_y); //origin dimension
	ox = sf_o(axis_x); //origin dimension
	ns = sf_n(axis_sou); //number of sources
	ng = sf_n(axis_rec); //number of receivers

	skip = (int) (Ts_field/dt);
	skip_ts=(int)(Ts/dt);
	int nsamples = (int)(nt/skip_ts)+1;
	sf_warning("\n time points: %d\n", nt);
	sf_warning("\n time samples: %d\n", nsamples);
	sf_warning("\n skip_ts: %d\n", skip_ts);
    // Write hypercube axes output
	sf_oaxa(Ffo,axis_z,1);
	sf_oaxa(Ffo,axis_y,2);
	sf_oaxa(Ffo,axis_x,3);
	sf_setn(axis_t,nt/skip); //Changes in axis length.
	sf_setd(axis_t,dt*skip); //Changes delta axis.
	sf_oaxa(Ffo,axis_t,4);   //Writes an axis.

	//Write gathers axes descriptions output
	//genera muestreo para trazas
	sf_axis axis_t2 = sf_iaxa(Fw,1); //first value

	sf_setn(axis_t2,nsamples); //Changes in axis length.
	sf_setd(axis_t2,dt*skip_ts); //Changes delta axis.

	sf_setd(axis_rec,1.0); //Changes delta axis.

	sf_oaxa(FgZ,axis_t2,1);   //(n1,o1,d1) Write the axis-1 (from input data)
	sf_oaxa(FgZ,axis_rec,2);  //Writes the axis-2, number of receivers
	sf_oaxa(FgZ,axis_sou,3);  //Writes the axis-3, number of sources

	sf_oaxa(FgY,axis_t2,1);   //(n1,o1,d1) Write the axis-1 (from input data)
	sf_oaxa(FgY,axis_rec,2);  //Writes the axis-2, number of receivers
	sf_oaxa(FgY,axis_sou,3);  //Writes the axis-3, number of sources

	sf_oaxa(FgX,axis_t2,1);   //(n1,o1,d1) Write the axis-1 (from input data)
	sf_oaxa(FgX,axis_rec,2);  //Writes the axis-2, number of receivers
	sf_oaxa(FgX,axis_sou,3);  //Writes the axis-3, number of sources

	//Write topography axes
	sf_oaxa(Ftop,axis_y,1);
	sf_oaxa(Ftop,axis_x,2);

	
	//read wavelet
  	float *wavelet=sf_floatalloc(nt);
  	sf_floatread(wavelet,nt,Fw);
	
   // read elastic model
	float *vp1=sf_floatalloc(nz1*nx1*ny1); 
    float *vs1=sf_floatalloc(nz1*nx1*ny1);
	float *rho1=sf_floatalloc(nz1*nx1*ny1); 
    sf_floatread(vp1,nz1*nx1*ny1,Fvp);
    sf_floatread(vs1,nz1*nx1*ny1,Fvs);
	sf_floatread(rho1,nz1*nx1*ny1,Frho);

	// expand the model
	ny = ny1 + 2*nb;
	nx = nx1 + 2*nb;
    nz = nz1 + nb;
    float *vp=sf_floatalloc(nz*nx*ny); 
    float *vs=sf_floatalloc(nz*nx*ny); 
    float *rho=sf_floatalloc(nz*nx*ny); 
    expand(vp,vp1,nz,nx,ny,nz1,nx1,ny1);
    expand(vs,vs1,nz,nx,ny,nz1,nx1,ny1);
    expand(rho,rho1,nz,nx,ny,nz1,nx1,ny1);

	//Array source receiver definition and reading
    float *sour=sf_floatalloc(3*ns); 
	sf_floatread(sour,3*ns,Fso);
	if (check_pos(sour,ns,ox,oy,oz,dx,dy,dz,nx1,ny1,nz1))
		sf_error("Sources position out of range");

    float *rece=sf_floatalloc(3*ng); 
	sf_floatread (rece,3*ng,Fre);
	if (check_pos(rece,ng,ox,oy,oz,dx,dy,dz,nx1,ny1,nz1))
		sf_error("Receivers position out of range");
	update_pos(rece,ng,nb,dx,dy);
      
	//Reciprocal of space sampling
	idx=1/dx; idy=1/dy; idz=1/dz;
   
    // Topography cdefinition
	int *top = sf_intalloc(nx*ny);
   	topography (vs, top, nx, ny, nz);
   
   // Definition surface and interior regions
	int cont_s, cont_i;
	indices_lengths(rho, nx, ny, nz, &cont_s, &cont_i);
    sf_warning("Surface points: %d\nInterior points: %d\n", cont_s, cont_i);
	int *ind_s = (int *)calloc(cont_s,sizeof(int));
	int *ind_i = (int *)calloc(cont_i,sizeof(int));
	indices_arrays(rho, nx, ny, nz, ind_s, ind_i);
	
	// --------- Receivers points -------------
	int cont_g=0;
	indices_length_rec(&cont_g, rece, ng, nx, idx);
	int *ind_g = sf_intalloc(cont_g);
	indices_array_rec(ind_g, rece, top, ng, nx, idx);
	
	// --------- CAMPOS EN CPU -------------
	float *traceVx = sf_floatalloc(nsamples*ng);
   	float *traceVy = sf_floatalloc(nsamples*ng);
   	float *traceVz = sf_floatalloc(nsamples*ng); 
	float *traceVxCPU = sf_floatalloc(cont_g); 
   	float *traceVyCPU = sf_floatalloc(cont_g);
	float *traceVzCPU = sf_floatalloc(cont_g);
	float *vzCPU = sf_floatalloc(nx*ny*nz);
  
   	// --------- PARAMETROS EN LA SUPERFICIE EN CPU --------------------------------------------------------------------
  	float *bxs = sf_floatalloc(cont_s);
	float *bys = sf_floatalloc(cont_s);
	float *bzs = sf_floatalloc(cont_s);
	float *mu_xys = sf_floatalloc(cont_s);
	float *mu_yzs = sf_floatalloc(cont_s);
	float *mu_zxs = sf_floatalloc(cont_s);
	float *n1_xxs = sf_floatalloc(cont_s);
	float *n2_xxs = sf_floatalloc(cont_s);
	float *n3_xxs = sf_floatalloc(cont_s);
	float *n1_yys = sf_floatalloc(cont_s);
	float *n2_yys = sf_floatalloc(cont_s);
	float *n3_yys = sf_floatalloc(cont_s);
	float *n1_zzs = sf_floatalloc(cont_s);
	float *n2_zzs = sf_floatalloc(cont_s);
	float *n3_zzs = sf_floatalloc(cont_s);

	// --------- CALCULO DE PARAMETROS EN LA SUPERFICIE EN CPU -------------------------------------------------------//
	parameter_fs(vp,      vs,      rho,
				 bxs,     bys,     bzs,
				 mu_xys,  mu_yzs,  mu_zxs,
				 n1_xxs,  n2_xxs,  n3_xxs,
				 n1_yys,  n2_yys,  n3_yys,
				 n1_zzs,  n2_zzs,  n3_zzs,
				 ind_s,	  cont_s,
				 nx,	  ny, nz);

	

	//------- PARAMETROS DE CALCULO DEL MODELADO GPU -----------------------------------------------------------------------
	float *vpGPU,*vsGPU,*rhoGPU;
	float *vxGPU,*vyGPU,*vzGPU;
	float *aGPU;
	float *sigmaxxGPU,*sigmayyGPU,*sigmazzGPU,*sigmaxyGPU,*sigmayzGPU,*sigmazxGPU;
	float *bxsGPU, *bysGPU, *bzsGPU;
	float *mu_xysGPU, *mu_yzsGPU, *mu_zxsGPU;
	float *n1_xxsGPU, *n2_xxsGPU, *n3_xxsGPU, *n1_yysGPU, *n2_yysGPU, *n3_yysGPU, *n1_zzsGPU, *n2_zzsGPU, *n3_zzsGPU;
	float *traceVxGPU, *traceVyGPU, *traceVzGPU, *receGPU;
	int   *topGPU;
	int   *ind_sGPU, *ind_iGPU, *ind_gGPU;


	// ------- MEMORIA creación dinamica de los punteros en GPU para vp,vs,rho ----------------------- //
	cudaMalloc(&vpGPU, (nx*ny*nz)*sizeof(float));
	cudaMalloc(&vsGPU, (nx*ny*nz)*sizeof(float));
	cudaMalloc(&rhoGPU,(nx*ny*nz)*sizeof(float));

	cudaMemcpy(vpGPU, vp, (nx*ny*nz)*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(vsGPU, vs, (nx*ny*nz)*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(rhoGPU, rho, (nx*ny*nz)*sizeof(float), cudaMemcpyHostToDevice);
	// ------- MEMORIA creación dinamica de los punteros en GPU campos VELOCIDADES (2) -------------------------------//
	cudaMalloc(&vxGPU,(nx*ny*nz)*sizeof(float));      cudaMemset(vxGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&vyGPU,(nx*ny*nz)*sizeof(float));	   cudaMemset(vyGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&vzGPU,(nx*ny*nz)*sizeof(float));	   cudaMemset(vzGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&sigmaxxGPU,(nx*ny*nz)*sizeof(float)); cudaMemset(sigmaxxGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&sigmayyGPU,(nx*ny*nz)*sizeof(float)); cudaMemset(sigmayyGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&sigmazzGPU,(nx*ny*nz)*sizeof(float)); cudaMemset(sigmazzGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&sigmaxyGPU,(nx*ny*nz)*sizeof(float)); cudaMemset(sigmaxyGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&sigmayzGPU,(nx*ny*nz)*sizeof(float)); cudaMemset(sigmayzGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&sigmazxGPU,(nx*ny*nz)*sizeof(float)); cudaMemset(sigmazxGPU,(float)0,(nx*ny*nz)*sizeof(float));
	cudaMalloc(&topGPU,(nx*ny)*sizeof(int));   	   cudaMemcpy(topGPU,top,sizeof(int)*(nx*ny),cudaMemcpyHostToDevice);

	// ------- MEMORIA creación dinamica de los punteros en GPU de parametros en superficie---------------------------//
	cudaMalloc(&bxsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&bysGPU,(cont_s)*sizeof(float));
	cudaMalloc(&bzsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&mu_xysGPU,(cont_s)*sizeof(float));
	cudaMalloc(&mu_yzsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&mu_zxsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n1_xxsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n2_xxsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n3_xxsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n1_yysGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n2_yysGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n3_yysGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n1_zzsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n2_zzsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&n3_zzsGPU,(cont_s)*sizeof(float));
	cudaMalloc(&ind_sGPU,cont_s*sizeof(int));
	cudaMalloc(&ind_iGPU,cont_i*sizeof(int));

	cudaMemcpy(bxsGPU,		bxs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(bysGPU,		bys,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(bzsGPU,		bzs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(mu_xysGPU,	mu_xys,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(mu_yzsGPU,	mu_yzs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(mu_zxsGPU,	mu_zxs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n1_xxsGPU,	n1_xxs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n2_xxsGPU,	n2_xxs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n3_xxsGPU,	n3_xxs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n1_yysGPU,	n1_yys,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n2_yysGPU,	n2_yys,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n3_yysGPU,	n3_yys,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n1_zzsGPU,	n1_zzs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n2_zzsGPU,	n2_zzs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(n3_zzsGPU,	n3_zzs,		cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(ind_sGPU,	ind_s,  	cont_s*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMemcpy(ind_iGPU,	ind_i,  	cont_i*sizeof(int),	cudaMemcpyHostToDevice);

  	 // ------- MEMORIA creación dinamica de los punteros en GPU campos auxiliares ABC---------------------- //
    float *coef = sf_floatalloc(nb);
    float *OxxGPU, *OxyGPU, *OxzGPU, *OyxGPU, *OyyGPU, *OyzGPU, *OzxGPU, *OzyGPU, *OzzGPU; 
	float *PxxGPU, *PxyGPU, *PxzGPU, *PyxGPU, *PyyGPU, *PyzGPU, *PzxGPU, *PzyGPU, *PzzGPU; 
    float *coefGPU;
    float *a;
	float R;
	//if(pml){
		init_pml(coef,nb,Vmax,dt,dx,ny,nz);
        cudaMalloc(&coefGPU,(ny)*sizeof(float));   
        cudaMemcpy(coefGPU,coef,sizeof(float)*(nb),cudaMemcpyHostToDevice);
		// X-direction PMLs aux. variables
		cudaMalloc(&OxxGPU,(2*nb*ny*nz)*sizeof(float));  cudaMemset(OxxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMalloc(&OyxGPU,(2*nb*ny*nz)*sizeof(float));  cudaMemset(OyxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMalloc(&OzxGPU,(2*nb*ny*nz)*sizeof(float));  cudaMemset(OzxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMalloc(&PxxGPU,(2*nb*ny*nz)*sizeof(float));  cudaMemset(PxxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMalloc(&PyxGPU,(2*nb*ny*nz)*sizeof(float));  cudaMemset(PyxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMalloc(&PzxGPU,(2*nb*ny*nz)*sizeof(float));  cudaMemset(PzxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		// Y-direction PMLs aux. variables
		cudaMalloc(&OxyGPU,(2*nb*nx*nz)*sizeof(float));  cudaMemset(OxyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMalloc(&OyyGPU,(2*nb*nx*nz)*sizeof(float));  cudaMemset(OyyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMalloc(&OzyGPU,(2*nb*nx*nz)*sizeof(float));  cudaMemset(OzyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMalloc(&PxyGPU,(2*nb*nx*nz)*sizeof(float));  cudaMemset(PxyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMalloc(&PyyGPU,(2*nb*nx*nz)*sizeof(float));  cudaMemset(PyyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMalloc(&PzyGPU,(2*nb*nx*nz)*sizeof(float));  cudaMemset(PzyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		// Z-direction PMLs aux. variables
		cudaMalloc(&OxzGPU,(nb*nx*ny)*sizeof(float));  cudaMemset(OxzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMalloc(&OyzGPU,(nb*nx*ny)*sizeof(float));  cudaMemset(OyzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMalloc(&OzzGPU,(nb*nx*ny)*sizeof(float));  cudaMemset(OzzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMalloc(&PxzGPU,(nb*nx*ny)*sizeof(float));  cudaMemset(PxzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMalloc(&PyzGPU,(nb*nx*ny)*sizeof(float));  cudaMemset(PyzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMalloc(&PzzGPU,(nb*nx*ny)*sizeof(float));  cudaMemset(PzzGPU, 0, sizeof(float)*(nb*nx*ny));
	//} else {
		a = (float *)calloc(nb,sizeof(float));
    	R = 1e-3;
    	init_abc(nb,Vmax,R,dt,dz,a);
    	cudaMalloc(&aGPU,(nb)*sizeof(float));   /*-*/cudaMemcpy(aGPU,a,sizeof(float)*(nb),cudaMemcpyHostToDevice);
	//}






	// // ------- MEMORIA creación dinamica de los punteros en GPU receiver points------------------------------- //
	cudaMalloc(&receGPU, 3*ng*sizeof(float));
	cudaMemcpy(receGPU, rece, 3*ng*sizeof(float),	cudaMemcpyHostToDevice);
	cudaMalloc(&ind_gGPU, cont_g*sizeof(int));
	cudaMemcpy(ind_gGPU, ind_g, cont_g*sizeof(int),	cudaMemcpyHostToDevice);
	cudaMalloc(&traceVxGPU,cont_g*sizeof(float)); cudaMemset(traceVxGPU,(float)0,(cont_g)*sizeof(float));
	cudaMalloc(&traceVyGPU,cont_g*sizeof(float)); cudaMemset(traceVyGPU,(float)0,(cont_g)*sizeof(float));
	cudaMalloc(&traceVzGPU,cont_g*sizeof(float)); cudaMemset(traceVzGPU,(float)0,(cont_g)*sizeof(float));

	// ----------- GPU LAYOUTS DESIGN ----------------------------------------------
   	int threads_i=512, threads_s=512;
	int threads_x=8, threads_y=8, threads_z=8;
	dim3 blockgeneral(threads_z,threads_y,threads_x);
	dim3 gridgeneral((nz-1)/threads_z+1,(ny-1)/threads_y+1,(nx-1)/threads_x+1);
	dim3 block_inside(threads_i);
	dim3 grid_inside((cont_i+threads_i-1)/threads_i);
	dim3 block_surface(threads_s);
	dim3 grid_surface((cont_s+threads_s-1)/threads_s);
	dim3 gridmod(threads_i);
	dim3 blockmod((nx*ny*nz-1)/threads_i+1);
	dim3 blockDim(8,8,8);
    dim3 gridDim((nx + blockDim.x - 1) / blockDim.x,(ny + blockDim.y - 1) / blockDim.y, (nz + blockDim.z - 1) / blockDim.z);
	dim3 blockpmlx(8,8,8);
    dim3 gridpmlx((nb + blockDim.x - 1) / blockDim.x,(ny + blockDim.y - 1) / blockDim.y, (nz + blockDim.z - 1) / blockDim.z);
	dim3 gridpmlx2((2*nb + blockDim.x - 1) / blockDim.x,(ny + blockDim.y - 1) / blockDim.y, (nz + blockDim.z - 1) / blockDim.z);
	dim3 blockpmly(8,8,8);
    dim3 gridpmly((nx + blockDim.x - 1) / blockDim.x,(nb + blockDim.y - 1) / blockDim.y, (nz + blockDim.z - 1) / blockDim.z);
	dim3 gridpmly2((nx + blockDim.x - 1) / blockDim.x,(2*nb + blockDim.y - 1) / blockDim.y, (nz + blockDim.z - 1) / blockDim.z);
	dim3 blockpmlz(8,8,8);
    dim3 gridpmlz((nx + blockDim.x - 1) / blockDim.x,(ny + blockDim.y - 1) / blockDim.y, (nb + blockDim.z - 1) / blockDim.z);

	//sf_warning("gridDim: (%d, %d, %d)\n", gridDim.x, gridDim.y, gridDim.z);

	dim3 block_inside_shared((nx*ny*nz-1)/threads_i+1);
	// ----------- PROPAGATION ---------------------------------------------
	double time_ini = time_measure();

	//begin source loop //

   	for (int shot=0; shot<ns; shot++){

		//--------- PRINT SOURCES LOCATIONS AND DIMENSION MODEL ---------------------------------------------------------------------------//
		Sxx = X(sour,shot)+nb*dx; 
		Syy = Y(sour,shot)+nb*dy; 
		Szz = Z(sour,shot);

		sf_warning("\n===== Model dimension ==============\n");
		sf_warning("nz=%d \t\tny=%d \t\tnx=%d  \n", nz, ny, nx);
		
		sf_warning("\n===== Source location ==============\n");
		sf_warning("Shot %d of %d\n", shot+1, ns);
		sf_warning("Sou_x=%.3f \t\tSou_y=%.3f \t\tSou_z=%.3f  \n", X(sour,shot), Y(sour,shot), Szz);

		cudaMemset(vxGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(vyGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(vzGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(sigmaxxGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(sigmayyGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(sigmazzGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(sigmaxyGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(sigmayzGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(sigmazxGPU, 0, sizeof(float)*(nx*ny*nz));
		cudaMemset(OxxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMemset(OyxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMemset(OzxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMemset(PxxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMemset(PyxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMemset(PzxGPU, 0, sizeof(float)*(2*nb*ny*nz));
		cudaMemset(OxyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMemset(OyyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMemset(OzyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMemset(PxyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMemset(PyyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMemset(PzyGPU, 0, sizeof(float)*(2*nb*nx*nz));
		cudaMemset(OxzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMemset(OyzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMemset(OzzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMemset(PxzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMemset(PyzGPU, 0, sizeof(float)*(nb*nx*ny));
		cudaMemset(PzzGPU, 0, sizeof(float)*(nb*nx*ny));
		memset(traceVx, 0, sizeof(float)*nsamples*ng );
		memset(traceVy, 0, sizeof(float)*nsamples*ng );
		memset(traceVz, 0, sizeof(float)*nsamples*ng );

		//begin time loop //
		for ( int it = 0; it < nt; ++it) {	//****************************************

			if(pml) {
				// update pml auxiliar variables

				pml_auxx<<<gridpmlx2,blockpmlx>>>(OxxGPU,OyxGPU,OzxGPU,PxxGPU,PyxGPU,PzxGPU,sigmaxxGPU,sigmaxyGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,coefGPU,topGPU,dt,idy,nb,nx,ny,nz);
				pml_auxy<<<gridpmly2,blockpmly>>>(OxyGPU,OyyGPU,OzyGPU,PxyGPU,PyyGPU,PzyGPU,sigmayyGPU,sigmaxyGPU,sigmayzGPU,vxGPU,vyGPU,vzGPU,coefGPU,topGPU,dt,idy,nb,nx,ny,nz);
				pml_auxz<<<gridpmlz,blockpmlz>>>(OxzGPU,OyzGPU,OzzGPU,PxzGPU,PyzGPU,PzzGPU,sigmazzGPU,sigmayzGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,coefGPU,dt,idy,nb,nx,ny,nz);     
				
				sigma_surface<<<grid_surface,block_surface>>>(vxGPU,vyGPU,vzGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,mu_xysGPU,mu_yzsGPU,mu_zxsGPU,n1_xxsGPU,n2_xxsGPU,n3_xxsGPU,n1_yysGPU,n2_yysGPU,n3_yysGPU,n1_zzsGPU,n2_zzsGPU,n3_zzsGPU,ind_sGPU,cont_s,dt,idx,idy,idz,nx,ny,nz);

				sigma_inside<<<grid_inside,block_inside>>>(vpGPU,vsGPU,rhoGPU,vxGPU,vyGPU,vzGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,ind_iGPU,cont_i,dt,idx,idy,idz,nx,ny,nz);
				
				//sigma_inside_shared<<<gridDim,blockDim>>>(vpGPU,vsGPU,rhoGPU,vxGPU,vyGPU,vzGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,topGPU,dt,idx,idy,idz,nx,ny,nz);

				source_insert<<<gridgeneral,blockgeneral>>>(wavelet[it], sigmaxxGPU, sigmayyGPU, sigmazzGPU, topGPU, idx, idy, idz, nx, ny, nz, Sxx, Syy, Szz);
				
				velocidad_surface<<<grid_surface,block_surface>>>(sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,bxsGPU,bysGPU,bzsGPU,ind_sGPU,cont_s,dt,idx,idy,idz,nx,ny,nz);

				velocidad_inside<<<grid_inside,block_inside>>>(rhoGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,ind_iGPU,cont_i,dt,idx,idy,idz,nx,ny,nz);

				pmlx_front<<<gridpmlx,blockpmlx>>>(vpGPU,vsGPU,rhoGPU,OxxGPU,OyxGPU,OzxGPU,PxxGPU,PyxGPU,PzxGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,topGPU,dt,nb,nx,ny,nz);
				pmlx_back<<<gridpmlx,blockpmlx>>>(vpGPU,vsGPU,rhoGPU,OxxGPU,OyxGPU,OzxGPU,PxxGPU,PyxGPU,PzxGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,topGPU,dt,nb,nx,ny,nz);
				pmly_left<<<gridpmly,blockpmly>>>(vpGPU,vsGPU,rhoGPU,OxyGPU,OyyGPU,OzyGPU,PxyGPU,PyyGPU,PzyGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,vxGPU,vyGPU,vzGPU,topGPU,dt,nb,nx,ny,nz);
				pmly_right<<<gridpmly,blockpmly>>>(vpGPU,vsGPU,rhoGPU,OxyGPU,OyyGPU,OzyGPU,PxyGPU,PyyGPU,PzyGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,vxGPU,vyGPU,vzGPU,topGPU,dt,nb,nx,ny,nz);
				pmlz_bottom<<<gridpmlz,blockpmlz>>>(vpGPU,vsGPU,rhoGPU,OxzGPU,OyzGPU,OzzGPU,PxzGPU,PyzGPU,PzzGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmayzGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,dt,nb,nx,ny,nz);  
			
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,vxGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,vyGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,vzGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmaxxGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmayyGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmazzGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmaxyGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmayzGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmazxGPU);
			} else {
				sigma_surface<<<grid_surface,block_surface>>>(vxGPU,vyGPU,vzGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,mu_xysGPU,mu_yzsGPU,mu_zxsGPU,n1_xxsGPU,n2_xxsGPU,n3_xxsGPU,n1_yysGPU,n2_yysGPU,n3_yysGPU,n1_zzsGPU,n2_zzsGPU,n3_zzsGPU,ind_sGPU,cont_s,dt,idx,idy,idz,nx,ny,nz);

				sigma_inside<<<grid_inside,block_inside>>>(vpGPU,vsGPU,rhoGPU,vxGPU,vyGPU,vzGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,ind_iGPU,cont_i,dt,idx,idy,idz,nx,ny,nz);
				
				source_insert<<<gridgeneral,blockgeneral>>>(wavelet[it], sigmaxxGPU, sigmayyGPU, sigmazzGPU, topGPU, idx, idy, idz, nx, ny, nz, Sxx, Syy, Szz);
				
				velocidad_surface<<<grid_surface,block_surface>>>(sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,bxsGPU,bysGPU,bzsGPU,ind_sGPU,cont_s,dt,idx,idy,idz,nx,ny,nz);

				velocidad_inside<<<grid_inside,block_inside>>>(rhoGPU,sigmaxxGPU,sigmayyGPU,sigmazzGPU,sigmaxyGPU,sigmayzGPU,sigmazxGPU,vxGPU,vyGPU,vzGPU,ind_iGPU,cont_i,dt,idx,idy,idz,nx,ny,nz);

				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,vxGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,vyGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,vzGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmaxxGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmayyGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmazzGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmaxyGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmayzGPU);
				// abc<<<blockmod,gridmod>>>(nx,ny,nz,nb,aGPU,sigmazxGPU);
			}

			// Geophone gather 
			if(it% skip_ts==0){
				gatherVx<<<grid_surface,block_surface>>>(traceVxGPU, vxGPU, receGPU, vsGPU, topGPU, ind_gGPU, cont_g, idx, idy, idz, nx, ny, nz);
				gatherVy<<<grid_surface,block_surface>>>(traceVyGPU, vyGPU, receGPU, vsGPU, topGPU, ind_gGPU, cont_g, idx, idy, idz, nx, ny, nz);
				gatherVz<<<grid_surface,block_surface>>>(traceVzGPU, vzGPU, receGPU, vsGPU, topGPU, ind_gGPU, cont_g, idx, idy, idz, nx, ny, nz);

				cudaMemcpy(traceVxCPU, traceVxGPU,(cont_g)*sizeof(float),cudaMemcpyDeviceToHost);
				cudaMemcpy(traceVyCPU, traceVyGPU,(cont_g)*sizeof(float),cudaMemcpyDeviceToHost);
				cudaMemcpy(traceVzCPU, traceVzGPU,(cont_g)*sizeof(float),cudaMemcpyDeviceToHost);

				for (int g=0; g< ng; g++){
				traceVx[g*nsamples+(it/skip_ts)]=-traceVxCPU[g]; //Change sign x-component
				traceVy[g*nsamples+(it/skip_ts)]=-traceVyCPU[g]; //Change sign y-component
				traceVz[g*nsamples+(it/skip_ts)]=traceVzCPU[g];
				}
			}

			// write wavefield to output
			if(it%skip==0){
				cudaMemcpy(vzCPU,vzGPU,(nx*ny*nz)*sizeof(float),cudaMemcpyDeviceToHost);
				window(vp1,vzCPU,nz1,nx1,ny1,nz,nx,ny);
				sf_floatwrite(vp1,nz1*nx1*ny1,Ffo);
			}
			sf_warning("%d%s;",it*100/nt,"%");
		} // end time loop

		//sf_floatwrite(field_vz,nx*ny*nz*(nt/skip),Ffo);	
		sf_floatwrite(traceVx,nsamples*ng,FgX);	
		sf_floatwrite(traceVy,nsamples*ng,FgY);	
		sf_floatwrite(traceVz,nsamples*ng,FgZ);	
	} // end source loop

	int topmax = top_max (vs, nx, ny, nz);
    float *top2 = (float *)calloc(nx1*ny1,sizeof(float));
    for ( i = 0; i < nx1; i++){
		for ( j = 0; j < ny1; j++){
			top2[i*ny1+j] = dz*(topmax-top[(i+nb)*ny+(j+nb)]);     //Topography in meters
		}
	} 
		
    sf_floatwrite( top2, nx1*ny1, Ftop );

	double time_end = time_measure();
	sf_warning("\nElapsed time is: %f\n", time_end-time_ini);

	sf_close();

	free(vzCPU);     
	free(vp);           free(vs);            free(rho);
	free(vp1);          free(vs1);           free(rho1);
  	free(bxs);          free(bys);           free(bzs);
  	free(mu_xys);       free(mu_yzs);        free(mu_zxs);
  	free(n1_xxs);       free(n2_xxs);        free(n3_xxs);
  	free(n1_yys);       free(n2_yys);        free(n3_yys);
  	free(n1_zzs);       free(n2_zzs);        free(n3_zzs);
	free(traceVx);      free(traceVy);       free(traceVz);
	free(traceVxCPU);   free(traceVyCPU);    free(traceVzCPU);
	free(ind_i);        free(ind_s);         free(ind_g);
  	free(top); 			free(top2);
	free(rece);         free(sour);          free(wavelet);

	cudaFree(vxGPU); 			cudaFree(vyGPU);            cudaFree(vzGPU);
	cudaFree(vpGPU); 			cudaFree(vsGPU);            cudaFree(rhoGPU);
  	cudaFree(sigmaxxGPU); 		cudaFree(sigmayyGPU);       cudaFree(sigmazzGPU);
  	cudaFree(sigmaxyGPU);      	cudaFree(sigmayzGPU);       cudaFree(sigmazxGPU);
  	cudaFree(bxsGPU);          	cudaFree(bysGPU);           cudaFree(bzsGPU);
  	cudaFree(mu_xysGPU);       	cudaFree(mu_yzsGPU);        cudaFree(mu_zxsGPU);
  	cudaFree(n1_xxsGPU);       	cudaFree(n2_xxsGPU);        cudaFree(n3_xxsGPU);
  	cudaFree(n1_yysGPU);       	cudaFree(n2_yysGPU);        cudaFree(n3_yysGPU);
  	cudaFree(n1_zzsGPU);       	cudaFree(n2_zzsGPU);        cudaFree(n3_zzsGPU);
	cudaFree(traceVxGPU);      	cudaFree(traceVyGPU);       cudaFree(traceVzGPU);
	cudaFree(ind_iGPU);        	cudaFree(ind_sGPU);         cudaFree(ind_gGPU);
  	cudaFree(topGPU);
	cudaFree(receGPU);


	free(coef); 
    //if(pml){
        cudaFree(OxxGPU); cudaFree(OxyGPU); cudaFree(OxzGPU); 
		cudaFree(OyxGPU); cudaFree(OyyGPU); cudaFree(OyzGPU); 
		cudaFree(OzxGPU); cudaFree(OzyGPU); cudaFree(OzzGPU); 
        cudaFree(PxxGPU); cudaFree(PxyGPU); cudaFree(PxzGPU); 
		cudaFree(PyxGPU); cudaFree(PyyGPU); cudaFree(PyzGPU); 
		cudaFree(PzxGPU); cudaFree(PzyGPU); cudaFree(PzzGPU); 
        cudaFree(coefGPU);
    //} else {
        free(a); cudaFree(aGPU);
    //}
   return (0);
}
