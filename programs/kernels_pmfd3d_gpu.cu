/*
 ============================================================================
 Name        : kernels_pmfd3d_gpu.cu
 Author      : Ivan Sánchez
 Version     :
 Copyright   :
 Description : Elastic Modeling including 3D Thread Layout, CPML reduced memory and complete vx field transfer.
 ============================================================================
 */

#define Id(A,i,j,k)  (A)[ (i)*ny*nz + (j)*nz + (k) ]
#define Idx(A,i,j,k) (A)[ (i)*ny*nz + (j)*nz + (k) ]
#define Idy(A,i,j,k) (A)[ (i)*2*nb*nz + (j)*nz + (k) ]
#define Idz(A,i,j,k) (A)[ (i)*ny*nb + (j)*nb + (k) ]


#define STENCIL_WIDTH 2
#define BLOCK_SIZE_X 8  // Adjust based on GPU architecture
#define BLOCK_SIZE_Y 8
#define BLOCK_SIZE_Z 8
#define BLOCK_SIZE 512
/*   FD Coefficients */
#define C1 9.0f/8.0f
#define C2 -1.0f/24.0f
/* backward  FD derivative stencils */
#define Dxb(A,i,j,k,s) (C2*( (A)[(i+1)*ny*nz+(j)*nz+(k)] - (A)[(i-2)*ny*nz+(j)*nz+(k)] ) +	\
  	      C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i-1)*ny*nz+(j)*nz+(k)] ) )*s
#define Dyb(A,i,j,k,s) (C2*( (A)[(i)*ny*nz+(j+1)*nz+(k)] - (A)[(i)*ny*nz+(j-2)*nz+(k)] ) +	\
  		   C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i)*ny*nz+(j-1)*nz+(k)] ) )*s
#define Dzb(A,i,j,k,s) (C2*( (A)[(i)*ny*nz+(j)*nz+(k+1)] - (A)[(i)*ny*nz+(j)*nz+(k-2)] ) +	\
  		   C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i)*ny*nz+(j)*nz+(k-1)] ) )*s

/* forward FD derivative stencils */
#define Dxf(A,i,j,k,s) (C2*((A)[(i+2)*ny*nz+(j)*nz+(k)] - (A)[(i-1)*ny*nz+(j)*nz+(k)] )+	\
  		   C1*((A)[(i+1)*ny*nz+(j)*nz+(k)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s
#define Dyf(A,i,j,k,s) (C2*((A)[(i)*ny*nz+(j+2)*nz+(k)] - (A)[(i)*ny*nz+(j-1)*nz+(k)] ) +	\
  		   C1*((A)[(i)*ny*nz+(j+1)*nz+(k)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s
#define Dzf(A,i,j,k,s) (C2*((A)[(i)*ny*nz+(j)*nz+(k+2)] - (A)[(i)*ny*nz+(j)*nz+(k-1)] ) +	\
  		   C1*((A)[(i)*ny*nz+(j)*nz+(k+1)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s

// Define shared memory indexing (consistent with `Id(...)`)
#define IdShared(A, i, j, k) (A)[(i) * (BLOCK_SIZE_Y+2*STENCIL_WIDTH) * (BLOCK_SIZE_Z+2*STENCIL_WIDTH) + (j) * (BLOCK_SIZE_Z+2*STENCIL_WIDTH) + (k)]

// Forward and backward finite difference macros for shared memory
#define Dxb_Shared(A, i, j, k) (C2 * (A[i + 1][j][k] - A[i - 2][j][k]) + \
                                C1 * (A[i][j][k] - A[i - 1][j][k]))

#define Dyb_Shared(A, i, j, k) (C2 * (A[i][j + 1][k] - A[i][j - 2][k]) + \
                                C1 * (A[i][j][k] - A[i][j - 1][k]))

#define Dzb_Shared(A, i, j, k) (C2 * (A[i][j][k + 1] - A[i][j][k - 2]) + \
                                C1 * (A[i][j][k] - A[i][j][k - 1]))

#define Dxf_Shared(A, i, j, k) (C2 * (A[i + 2][j][k] - A[i - 1][j][k]) + \
                                C1 * (A[i + 1][j][k] - A[i][j][k]))

#define Dyf_Shared(A, i, j, k) (C2 * (A[i][j + 2][k] - A[i][j - 1][k]) + \
                                C1 * (A[i][j + 1][k] - A[i][j][k]))

#define Dzf_Shared(A, i, j, k) (C2 * (A[i][j][k + 2] - A[i][j][k - 1]) + \
                                C1 * (A[i][j][k + 1] - A[i][j][k]))




// ************ DECLARACION VARIABLES EN GPU DE FORMA ESTATICA **************************    //


/*******************************************/
/* Update setresses from velocities
   in the surface region*/
/*******************************************/
__global__ void sigma_surface(float *vx, 
                              float *vy, 
                              float *vz, 
                              float *sigmaxx, 
                              float *sigmayy, 
                              float *sigmazz, 
                              float *sigmaxy, 
                              float *sigmayz, 
                              float *sigmazx, 
                              float *mu_xys, 
                              float *mu_yzs, 
                              float *mu_zxs, 
                              float *n1_xxs, 
                              float *n2_xxs, 
                              float *n3_xxs, 
                              float *n1_yys, 
                              float *n2_yys, 
                              float *n3_yys, 
                              float *n1_zzs, 
                              float *n2_zzs, 
                              float *n3_zzs, 
                              int *ind_s, 
                              int cont_s, 
                              float dt, 
                              float idx, 
                              float idy, 
                              float idz, 
                              int nx, 
                              int ny, 
                              int nz)
{
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	int i, j, k;
	if ( ind<cont_s ){
	   i= ind_s[ind]/(ny*nz);
	   j= (ind_s[ind]-(i)*(ny*nz))/nz;
	   k= ind_s[ind]- ((i)*ny*nz + (j)*nz);
      Id(sigmaxx,i,j,k) += (dt)*(n1_xxs[ind]*Dxb(vx,i,j,k,idx) + n2_xxs[ind]*Dyb(vy,i,j,k,idy) + n3_xxs[ind]*Dzb(vz,i,j,k,idz));
      Id(sigmayy,i,j,k) += (dt)*(n1_yys[ind]*Dxb(vx,i,j,k,idx) + n2_yys[ind]*Dyb(vy,i,j,k,idy) + n3_yys[ind]*Dzb(vz,i,j,k,idz));
      Id(sigmazz,i,j,k) += (dt)*(n1_zzs[ind]*Dxb(vx,i,j,k,idx) + n2_zzs[ind]*Dyb(vy,i,j,k,idy) + n3_zzs[ind]*Dzb(vz,i,j,k,idz));
      Id(sigmaxy,i,j,k) += (dt)*mu_xys[ind]*( Dxf(vy,i,j,k,idx) + Dyf(vx,i,j,k,idy) );
      Id(sigmayz,i,j,k) += (dt)*mu_yzs[ind]*( Dyf(vz,i,j,k,idy) + Dzf(vy,i,j,k,idz) );
      Id(sigmazx,i,j,k) += (dt)*mu_zxs[ind]*( Dxf(vz,i,j,k,idx) + Dzf(vx,i,j,k,idz) );
	} 
   else {
	}
}

/*******************************************/
/* Update setresses from velocities
   in the interior region*/
/*******************************************/
__global__ void sigma_inside(float *velp,
                             float *vels,
                             float *rho,
                             float *vx, 
                             float *vy, 
                             float *vz, 
                             float *sigmaxx, 
                             float *sigmayy, 
                             float *sigmazz, 
                             float *sigmaxy, 
                             float *sigmayz, 
                             float *sigmazx, 
                             int *ind_i, 
                             int cont_i, 
                             float dt, 
                             float idx, 
                             float idy, 
                             float idz, 
                             int nx, 
                             int ny, 
                             int nz
                             )
{
   int ind = threadIdx.x + blockIdx.x*blockDim.x;
	int i, j, k;
	float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_jk, mu_xy, mu_yz, mu_zx;
	float n1_xx, n2_xx, n3_xx, n1_yy, n2_yy, n3_yy, n1_zz, n2_zz, n3_zz;
	if ( ind<cont_i ){
		i= ind_i[ind]/(ny*nz);
		j= (ind_i[ind]-(i)*(ny*nz))/nz;
		k= ind_i[ind]- ((i)*ny*nz + (j)*nz);
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
		lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;

		mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k);
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k);
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1);
		mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k);
		mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1);
		mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1);
		mu_xy = 1/(0.25/mu+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
		mu_yz = 1/(0.25/mu+0.25/mu_j+0.25/mu_k+0.25/mu_jk);
		mu_zx = 1/(0.25/mu+0.25/mu_i+0.25/mu_k+0.25/mu_ik);

		n1_xx=lambda+2*mu;  n2_xx=lambda;         n3_xx=lambda;
		n1_yy=lambda;       n2_yy=lambda+2*mu;    n3_yy=lambda;
		n1_zz=lambda;       n2_zz=lambda;         n3_zz=lambda+2*mu;

		Id(sigmaxx,i,j,k) += (dt)*(n1_xx*Dxb(vx,i,j,k,idx) + n2_xx*Dyb(vy,i,j,k,idy) + n3_xx*Dzb(vz,i,j,k,idz));
		Id(sigmayy,i,j,k) += (dt)*(n1_yy*Dxb(vx,i,j,k,idx) + n2_yy*Dyb(vy,i,j,k,idy) + n3_yy*Dzb(vz,i,j,k,idz));
		Id(sigmazz,i,j,k) += (dt)*(n1_zz*Dxb(vx,i,j,k,idx) + n2_zz*Dyb(vy,i,j,k,idy) + n3_zz*Dzb(vz,i,j,k,idz));
		Id(sigmaxy,i,j,k) += (dt)*mu_xy*( Dxf(vy,i,j,k,idx) + Dyf(vx,i,j,k,idy) );
		Id(sigmayz,i,j,k) += (dt)*mu_yz*( Dyf(vz,i,j,k,idy) + Dzf(vy,i,j,k,idz) );
		Id(sigmazx,i,j,k) += (dt)*mu_zx*( Dxf(vz,i,j,k,idx) + Dzf(vx,i,j,k,idz) );
   } 
   else {
	}
}



__global__ void sigma_inside_shared(float *velp,
                                    float *vels,
                                    float *rho,
                                    float *vx, 
                                    float *vy, 
                                    float *vz, 
                                    float *sigmaxx, 
                                    float *sigmayy, 
                                    float *sigmazz, 
                                    float *sigmaxy, 
                                    float *sigmayz, 
                                    float *sigmazx, 
                                    int *top,   // topography
                                    float dt, 
                                    float idx, 
                                    float idy, 
                                    float idz, 
                                    int nx, 
                                    int ny, 
                                    int nz) 
{

    // Thread indices
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int k = blockIdx.z * blockDim.z + threadIdx.z;

    // Shared memory
    __shared__ float s_vx[BLOCK_SIZE_X + 2 * STENCIL_WIDTH][BLOCK_SIZE_Y+ 2 * STENCIL_WIDTH][BLOCK_SIZE_Z+ 2 * STENCIL_WIDTH];
    __shared__ float s_vy[BLOCK_SIZE_X + 2 * STENCIL_WIDTH][BLOCK_SIZE_Y+ 2 * STENCIL_WIDTH][BLOCK_SIZE_Z+ 2 * STENCIL_WIDTH];
    __shared__ float s_vz[BLOCK_SIZE_X + 2 * STENCIL_WIDTH][BLOCK_SIZE_Y+ 2 * STENCIL_WIDTH][BLOCK_SIZE_Z+ 2 * STENCIL_WIDTH];

    // Load velocity components into shared memory
	int s_i = threadIdx.x+STENCIL_WIDTH;
    int s_j = threadIdx.y+STENCIL_WIDTH;
    int s_k = threadIdx.z+STENCIL_WIDTH;

	float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_jk, mu_xy, mu_yz, mu_zx;
	float n1_xx, n2_xx, n3_xx, n1_yy, n2_yy, n3_yy, n1_zz, n2_zz, n3_zz;

    //if ( i>1 && i < nx-2 && j>1 && j<ny-2 && k>1 && k<nz-2) {
	if ( i < nx && j<ny && k<nz) {

        s_vx[s_i][s_j][s_k] = Id(vx, i, j, k);
        s_vy[s_i][s_j][s_k] = Id(vy, i, j, k);
        s_vz[s_i][s_j][s_k] = Id(vz, i, j, k);

        // Load halo regions
        if (s_i < 2*STENCIL_WIDTH && i > 1) {
            s_vx[s_i-STENCIL_WIDTH][s_j][s_k] = Id(vx, i - STENCIL_WIDTH, j, k);
			s_vy[s_i-STENCIL_WIDTH][s_j][s_k] = Id(vy, i - STENCIL_WIDTH, j, k);
			s_vz[s_i-STENCIL_WIDTH][s_j][s_k] = Id(vz, i - STENCIL_WIDTH, j, k);
        }
		if (s_i >= BLOCK_SIZE_X && i < nx-2) {
            s_vx[s_i+ STENCIL_WIDTH][s_j][s_k] = Id(vx, i + STENCIL_WIDTH, j, k);
			s_vy[s_i+ STENCIL_WIDTH][s_j][s_k] = Id(vy, i + STENCIL_WIDTH, j, k);
			s_vz[s_i+ STENCIL_WIDTH][s_j][s_k] = Id(vz, i + STENCIL_WIDTH, j, k);
        }
        if (s_j < 2*STENCIL_WIDTH && j > 1) {
            s_vx[s_i][s_j-STENCIL_WIDTH][s_k] = Id(vx, i, j- STENCIL_WIDTH, k);
			s_vy[s_i][s_j-STENCIL_WIDTH][s_k] = Id(vy, i, j- STENCIL_WIDTH, k);
			s_vz[s_i][s_j-STENCIL_WIDTH][s_k] = Id(vz, i, j- STENCIL_WIDTH, k);
        }
		if (s_j >= BLOCK_SIZE_Y && j < ny-2) {
            s_vx[s_i][s_j+STENCIL_WIDTH][s_k] = Id(vx, i, j+ STENCIL_WIDTH, k);
			s_vy[s_i][s_j+STENCIL_WIDTH][s_k] = Id(vy, i, j+ STENCIL_WIDTH, k);
			s_vz[s_i][s_j+STENCIL_WIDTH][s_k] = Id(vz, i, j+ STENCIL_WIDTH, k);
        }
		if (s_k < 2*STENCIL_WIDTH && k > 1) {
            s_vx[s_i][s_j][s_k-STENCIL_WIDTH] = Id(vx, i, j, k- STENCIL_WIDTH);
			s_vy[s_i][s_j][s_k-STENCIL_WIDTH] = Id(vy, i, j, k- STENCIL_WIDTH);
			s_vz[s_i][s_j][s_k-STENCIL_WIDTH] = Id(vz, i, j, k- STENCIL_WIDTH);
        }
		if (s_k >= BLOCK_SIZE_Z && k < nz-2) { 
            s_vx[s_i][s_j][s_k+ STENCIL_WIDTH] = Id(vx, i, j, k+ STENCIL_WIDTH);
			s_vy[s_i][s_j][s_k+ STENCIL_WIDTH] = Id(vy, i, j, k+ STENCIL_WIDTH);
			s_vz[s_i][s_j][s_k+ STENCIL_WIDTH] = Id(vz, i, j, k+ STENCIL_WIDTH);
        }
	}
	//Synchronize threads
    __syncthreads();

	// Compute stress updates using shared memory
	if ( i>1 && i < nx-2 && j>1 && j<ny-2 && k>1 && k<nz-2) {

		float inZone = (float)(k > top[(i) * ny + j]);
		float eps = (1.0f-inZone);
		// Compute material properties
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k) +eps;
		lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;

		mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k)+eps;
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k)+eps;
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1)+eps;
		mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k)+eps;
		mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1)+eps;
		mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1)+eps;
		mu_xy = 1/(0.25/mu+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
		mu_yz = 1/(0.25/mu+0.25/mu_j+0.25/mu_k+0.25/mu_jk);
		mu_zx = 1/(0.25/mu+0.25/mu_i+0.25/mu_k+0.25/mu_ik);

		n1_xx=lambda+2*mu;  n2_xx=lambda;         n3_xx=lambda;
		n1_yy=lambda;       n2_yy=lambda+2*mu;    n3_yy=lambda;
		n1_zz=lambda;       n2_zz=lambda;         n3_zz=lambda+2*mu;

		Id(sigmaxx, i, j, k) +=  dt * inZone * (
			n1_xx * Dxb_Shared(s_vx, s_i, s_j, s_k) * idx +
			n2_xx * Dyb_Shared(s_vy, s_i, s_j, s_k) * idy +
			n3_xx * Dzb_Shared(s_vz, s_i, s_j, s_k) * idz
		);

		Id(sigmayy, i, j, k) += dt * inZone *  (
			n1_yy * Dxb_Shared(s_vx, s_i, s_j, s_k) * idx +
			n2_yy * Dyb_Shared(s_vy, s_i, s_j, s_k) * idy +
			n3_yy * Dzb_Shared(s_vz, s_i, s_j, s_k) * idz
		);

		Id(sigmazz, i, j, k) += dt * inZone *  (
			n1_zz * Dxb_Shared(s_vx, s_i, s_j, s_k) * idx +
			n2_zz * Dyb_Shared(s_vy, s_i, s_j, s_k) * idy +
			n3_zz * Dzb_Shared(s_vz, s_i, s_j, s_k) * idz
		);

		Id(sigmaxy, i, j, k) += dt * mu_xy * inZone *  (
			Dxf_Shared(s_vy, s_i, s_j, s_k) * idx +
			Dyf_Shared(s_vx, s_i, s_j, s_k) * idy
		);

		Id(sigmayz, i, j, k) += dt * mu_yz * inZone *  (
			Dyf_Shared(s_vz, s_i, s_j, s_k) * idy +
			Dzf_Shared(s_vy, s_i, s_j, s_k) * idz
		);

		Id(sigmazx, i, j, k) += dt * mu_zx * inZone *  (
			Dxf_Shared(s_vz, s_i, s_j, s_k) * idx +
			Dzf_Shared(s_vx, s_i, s_j, s_k) * idz
		);
	}
	
}


/*******************************************/
/* Insert source*/
/*******************************************/
__global__ 
void source_insert(float source_it, 
				float *sigmaxx,  float *sigmayy, float *sigmazz,
				int *top, 
				float idx,       float idy,      float idz, 
				int nx,         int ny,         int nz, 
				float Sx,        float Sy,       float Sz){
	//int index;
	float lx,ly,lz,dx,dy,dz;
	float w_tlf,w_tlb,w_trf,w_trb,w_blf,w_blb,w_brf,w_brb;
	int Ix,Iy,Iz;
	int k = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	int i = threadIdx.z + blockIdx.z*blockDim.z;


	lx = (Sx)*idx;
	ly = (Sy)*idy;
	Ix = int(lx), Iy = int(ly);
	lz = (Sz)*idz + top[Ix*ny+Iy];
	Iz = int(lz);
	dx = lx-Ix, dy = ly-Iy, dz = lz-Iz;

	if (i == Ix && j == Iy && k == Iz ){

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		Id(sigmaxx,i,j,k) += w_tlf*source_it;      
		Id(sigmaxx,i+1,j,k) += w_tlb*source_it;   
		Id(sigmaxx,i,j+1,k) += w_trf*source_it;      
		Id(sigmaxx,i+1,j+1,k) += w_trb*source_it;    
		Id(sigmaxx,i,j,k+1) += w_blf*source_it;       
		Id(sigmaxx,i+1,j,k+1) += w_blb*source_it;    
		Id(sigmaxx,i,j+1,k+1) += w_brf*source_it;     
		Id(sigmaxx,i+1,j+1,k+1) += w_brb*source_it;   

		Id(sigmayy,i,j,k) += w_tlf*source_it;         
		Id(sigmayy,i+1,j,k) += w_tlb*source_it;      
		Id(sigmayy,i,j+1,k) += w_trf*source_it;       
		Id(sigmayy,i+1,j+1,k) += w_trb*source_it;    
		Id(sigmayy,i,j,k+1) += w_blf*source_it;       
		Id(sigmayy,i+1,j,k+1) += w_blb*source_it;    
		Id(sigmayy,i,j+1,k+1) += w_brf*source_it;     
		Id(sigmayy,i+1,j+1,k+1) += w_brb*source_it;   

		Id(sigmazz,i,j,k) += w_tlf*source_it;         
		Id(sigmazz,i+1,j,k) += w_tlb*source_it;      
		Id(sigmazz,i,j+1,k) += w_trf*source_it;       
		Id(sigmazz,i+1,j+1,k) += w_trb*source_it;    
		Id(sigmazz,i,j,k+1) += w_blf*source_it;       
		Id(sigmazz,i+1,j,k+1) += w_blb*source_it;    
		Id(sigmazz,i,j+1,k+1) += w_brf*source_it;     
		Id(sigmazz,i+1,j+1,k+1) += w_brb*source_it;   
	}
}

/*******************************************/
/* Update velocities from setresses
   in the surface region*/
/*******************************************/
__global__ void velocidad_surface(float *sigmaxx,
                                  float *sigmayy,
                                  float *sigmazz,
                                  float *sigmaxy,
                                  float *sigmayz,
                                  float *sigmazx, 
                                  float *vx,
                                  float *vy,
                                  float *vz, 
                                  float *bxs, 
                                  float *bys, 
                                  float *bzs, 
                                  int *ind_s, 
                                  int cont_s, 
                                  float dt, 
                                  float idx, 
                                  float idy, 
                                  float idz, 
                                  int nx, 
                                  int ny, 
                                  int nz
                                  )
{
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	int i, j, k;
	if ( ind<cont_s ){
	   i= ind_s[ind]/(ny*nz);
	   j= (ind_s[ind]-(i)*(ny*nz))/nz;
	   k= ind_s[ind]- ((i)*ny*nz + (j)*nz);
      Id(vx,i,j,k) += (dt)*bxs[ind]*( Dxf(sigmaxx,i,j,k,idx) +  Dyb(sigmaxy,i,j,k,idy) + Dzb(sigmazx,i,j,k,idz) );
      Id(vy,i,j,k) += (dt)*bys[ind]*( Dxb(sigmaxy,i,j,k,idx) +  Dyf(sigmayy,i,j,k,idy) + Dzb(sigmayz,i,j,k,idz) );
      Id(vz,i,j,k) += (dt)*bzs[ind]*( Dxb(sigmazx,i,j,k,idx) +  Dyb(sigmayz,i,j,k,idy) + Dzf(sigmazz,i,j,k,idz) );
   } 
   else {
   }
}

/*******************************************/
/* Update velocities from setresses
   in the interior region*/
/*******************************************/
__global__ void velocidad_inside(float *rho,
                                 float *sigmaxx,
                                 float *sigmayy,
                                 float *sigmazz,
                                 float *sigmaxy,
                                 float *sigmayz,
                                 float *sigmazx, 
                                 float *vx,
                                 float *vy,
                                 float *vz, 
                                 int *ind_i, 
                                 int cont_i, 
                                 float dt, 
                                 float idx, 
                                 float idy, 
                                 float idz, 
                                 int nx, 
                                 int ny, 
                                 int nz
                                 )
{
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	int i, j, k;
	float b, b_i, b_j, b_k, bx, by, bz;
	if ( ind<cont_i){
	   i= ind_i[ind]/(ny*nz);
	   j= (ind_i[ind]-(i)*(ny*nz))/nz;
	   k= ind_i[ind]- ((i)*ny*nz + (j)*nz);

      b  = 1/Id(rho,i,j,k);
      b_i= 1/Id(rho,i+1,j,k);
      b_j= 1/Id(rho,i,j+1,k);
      b_k= 1/Id(rho,i,j,k+1);

      bx=2*b*b_i/(b+b_i);
      by=2*b*b_j/(b+b_j);
      bz=2*b*b_k/(b+b_k);

      Id(vx,i,j,k) += (dt)*bx*( Dxf(sigmaxx,i,j,k,idx) +  Dyb(sigmaxy,i,j,k,idy) + Dzb(sigmazx,i,j,k,idz) );
      Id(vy,i,j,k) += (dt)*by*( Dxb(sigmaxy,i,j,k,idx) +  Dyf(sigmayy,i,j,k,idy) + Dzb(sigmayz,i,j,k,idz) );
      Id(vz,i,j,k) += (dt)*bz*( Dxb(sigmazx,i,j,k,idx) +  Dyb(sigmayz,i,j,k,idy) + Dzf(sigmazz,i,j,k,idz) );
   } 
   else {
   }
}


/*******************************************/
/* Apply ABC */
/*******************************************/

__global__ void abc(int nx, 
					int ny, 
                    int nz, 
                    int bound, 
                    float *coef, 
                    float *v)
{
    int ind = threadIdx.x + blockIdx.x*blockDim.x;
	int i, j, k;
	
    if ( ind<nx*ny*nz ){

	i= ind/(ny*nz);
	j= (ind-(i)*(ny*nz))/nz;
	k= ind- ((i)*ny*nz + (j)*nz);

        if ( i < bound ) {
            Id(v,i,j,k) *= coef[i];             //abc left side
            Id(v,(nx-1)-i,j,k) *= coef[i];      //abc right side
        }
		if ( j < bound ) {
            Id(v,i,j,k) *= coef[j];             //abc left side
            Id(v,i,(ny-1)-j,k) *= coef[j];      //abc right side
        }
        if ( k < bound) {
            //Id(v,i,j) *= coef[j];       //abc top side
            Id(v,i,j,(nz-1)-k) *= coef[k]; //abc bottom side
        }
    }
}



/*******************************************/
/* PML auxiliar fields update */
/*******************************************/

__global__ void pml_auxx(float *Oxx, 
                        float *Oyx,
						float *Ozx, 
                        float *Pxx, 
                        float *Pyx,
						float *Pzx, 
                        float *sigmaxx,
                        float *sigmaxy,
						float *sigmazx,
						float *vx,
                        float *vy,
                        float *vz,
                        float *cx,
                        int *top,
                        float dt, 
                        float idx, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
    if ( i>1 && i < nb && j>1 && j<ny-2 && k>1 && k<nz-2){
		Idx(Oxx,i,j,k) = cx[i]*Idx(Oxx,i,j,k) + (1-cx[i])*Dxf(sigmaxx,i,j,k,idx);
		Idx(Oyx,i,j,k) = cx[i]*Idx(Oyx,i,j,k) + (1-cx[i])*Dxb(sigmaxy,i,j,k,idx);
		Idx(Ozx,i,j,k) = cx[i]*Idx(Ozx,i,j,k) + (1-cx[i])*Dxb(sigmazx,i,j,k,idx);
		Idx(Pxx,i,j,k) = cx[i]*Idx(Pxx,i,j,k) + (1-cx[i])*Dxb(vx,i,j,k,idx);
		Idx(Pyx,i,j,k) = cx[i]*Idx(Pyx,i,j,k) + (1-cx[i])*Dxf(vy,i,j,k,idx);
		Idx(Pzx,i,j,k) = cx[i]*Idx(Pzx,i,j,k) + (1-cx[i])*Dxf(vz,i,j,k,idx);

		Idx(Oxx,2*nb-1-i,j,k) = cx[i]*Idx(Oxx,2*nb-1-i,j,k) + (1-cx[i])*Dxf(sigmaxx,nx-1-i,j,k,idx);
		Idx(Oyx,2*nb-1-i,j,k) = cx[i]*Idx(Oyx,2*nb-1-i,j,k) + (1-cx[i])*Dxb(sigmaxy,nx-1-i,j,k,idx);
		Idx(Ozx,2*nb-1-i,j,k) = cx[i]*Idx(Ozx,2*nb-1-i,j,k) + (1-cx[i])*Dxb(sigmazx,nx-1-i,j,k,idx);
		Idx(Pxx,2*nb-1-i,j,k) = cx[i]*Idx(Pxx,2*nb-1-i,j,k) + (1-cx[i])*Dxb(vx,nx-1-i,j,k,idx);
		Idx(Pyx,2*nb-1-i,j,k) = cx[i]*Idx(Pyx,2*nb-1-i,j,k) + (1-cx[i])*Dxf(vy,nx-1-i,j,k,idx);
		Idx(Pzx,2*nb-1-i,j,k) = cx[i]*Idx(Pzx,2*nb-1-i,j,k) + (1-cx[i])*Dxf(vz,nx-1-i,j,k,idx);
    }
}

__global__ void pml_auxy(float *Oxy, 
                        float *Oyy,
						float *Ozy, 
                        float *Pxy, 
                        float *Pyy,
						float *Pzy, 
                        float *sigmayy,
                        float *sigmaxy,
						float *sigmayz,
						float *vx,
                        float *vy,
                        float *vz,
                        float *cy,
                        int *top,
                        float dt, 
                        float idy, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
    if ( i>1 && i < nx-2 && j>1 && j<nb && k>1 && k<nz-2){
		Idy(Oxy,i,j,k) = cy[j]*Idy(Oxy,i,j,k) + (1-cy[j])*Dyb(sigmaxy,i,j,k,idy);
		Idy(Oyy,i,j,k) = cy[j]*Idy(Oyy,i,j,k) + (1-cy[j])*Dyf(sigmayy,i,j,k,idy);
		Idy(Ozy,i,j,k) = cy[j]*Idy(Ozy,i,j,k) + (1-cy[j])*Dyb(sigmayz,i,j,k,idy);
		Idy(Pxy,i,j,k) = cy[j]*Idy(Pxy,i,j,k) + (1-cy[j])*Dyf(vx,i,j,k,idy);
		Idy(Pyy,i,j,k) = cy[j]*Idy(Pyy,i,j,k) + (1-cy[j])*Dyb(vy,i,j,k,idy);
		Idy(Pzy,i,j,k) = cy[j]*Idy(Pzy,i,j,k) + (1-cy[j])*Dyf(vz,i,j,k,idy);

		Idy(Oxy,i,2*nb-1-j,k) = cy[j]*Idy(Oxy,i,2*nb-1-j,k) + (1-cy[j])*Dyb(sigmaxy,i,ny-1-j,k,idy);
		Idy(Oyy,i,2*nb-1-j,k) = cy[j]*Idy(Oyy,i,2*nb-1-j,k) + (1-cy[j])*Dyf(sigmayy,i,ny-1-j,k,idy);
		Idy(Ozy,i,2*nb-1-j,k) = cy[j]*Idy(Ozy,i,2*nb-1-j,k) + (1-cy[j])*Dyb(sigmayz,i,ny-1-j,k,idy);
		Idy(Pxy,i,2*nb-1-j,k) = cy[j]*Idy(Pxy,i,2*nb-1-j,k) + (1-cy[j])*Dyf(vx,i,ny-1-j,k,idy);
		Idy(Pyy,i,2*nb-1-j,k) = cy[j]*Idy(Pyy,i,2*nb-1-j,k) + (1-cy[j])*Dyb(vy,i,ny-1-j,k,idy);
		Idy(Pzy,i,2*nb-1-j,k) = cy[j]*Idy(Pzy,i,2*nb-1-j,k) + (1-cy[j])*Dyf(vz,i,ny-1-j,k,idy);
    }
}


__global__ void pml_auxz(float *Oxz, 
                        float *Oyz,
						float *Ozz, 
                        float *Pxz, 
                        float *Pyz,
						float *Pzz, 
                        float *sigmazz,
                        float *sigmayz,
						float *sigmazx,
						float *vx,
                        float *vy,
                        float *vz,
                        float *cz,
                        float dt, 
                        float idz, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
    if ( i>1 && i < nx-2 && j>1 && j<ny-2 && k>1 && k<nb){
		Idz(Oxz,i,j,nb-1-k) = cz[k]*Idz(Oxz,i,j,nb-1-k) + (1-cz[k])*Dzb(sigmazx,i,j,nz-1-k,idz);
		Idz(Oyz,i,j,nb-1-k) = cz[k]*Idz(Oyz,i,j,nb-1-k) + (1-cz[k])*Dzb(sigmayz,i,j,nz-1-k,idz);
		Idz(Ozz,i,j,nb-1-k) = cz[k]*Idz(Ozz,i,j,nb-1-k) + (1-cz[k])*Dzf(sigmazz,i,j,nz-1-k,idz);
		Idz(Pxz,i,j,nb-1-k) = cz[k]*Idz(Pxz,i,j,nb-1-k) + (1-cz[k])*Dzf(vx,i,j,nz-1-k,idz);
		Idz(Pyz,i,j,nb-1-k) = cz[k]*Idz(Pyz,i,j,nb-1-k) + (1-cz[k])*Dzf(vy,i,j,nz-1-k,idz);
		Idz(Pzz,i,j,nb-1-k) = cz[k]*Idz(Pzz,i,j,nb-1-k) + (1-cz[k])*Dzb(vz,i,j,nz-1-k,idz);
    }
}

/*******************************************/
/* PML application */
/*******************************************/

__global__ void pmlx_front(float *velp,
                        float *vels,
                        float *rho,
                        float *Oxx, 
                        float *Oyx,
						float *Ozx, 
                        float *Pxx, 
                        float *Pyx,
						float *Pzx, 
						float *sigmaxx,
                        float *sigmayy,
						float *sigmazz,
                        float *sigmaxy,
						float *sigmazx,
						float *vx,
                        float *vy,
                        float *vz,
                        int *top,
                        float dt, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
    float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_xy, mu_zx;
    float n1_xx, n1_yy, n1_zz;
    float b, b_i, b_j, b_k, bx, by, bz;
	float eps, eps_i, eps_j, eps_k, eps_ij, eps_ik;
	float pmlZone;
    if ( i>1 && i < nb && j>1 && j < ny-2 && k>1 && k < nz-2){
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
      	lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;
		eps = (Id(vels,i,j,k)==0);
		eps_i =  float(Id(vels,i+1,j,k)==0);
		eps_j =  float(Id(vels,i,j+1,k)==0);
		eps_k =  float(Id(vels,i,j,k+1)==0) ;
		eps_ij = float(Id(vels,i+1,j+1,k)==0);
		eps_ik = float(Id(vels,i+1,j,k+1)==0) ;
		pmlZone = float(Id(vels,i,j,k-2)>0);
	   	mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k)+eps_i;
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k)+eps_j;
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1)+eps_k;
		mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k)+eps_ij;
		mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1)+eps_ik;
		mu_xy = 1/(0.25/(mu+eps)+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
		mu_zx = 1/(0.25/(mu+eps)+0.25/mu_i+0.25/mu_k+0.25/mu_ik);

		n1_xx=lambda+2*mu;  
		n1_yy=lambda;     
		n1_zz=lambda;       

		b  = 1/(Id(rho,i,j,k)+eps);
		b_i= 1/(Id(rho,i+1,j,k)+eps_i);
		b_j= 1/(Id(rho,i,j+1,k)+eps_j);
		b_k= 1/(Id(rho,i,j,k+1)+eps_k);

		bx=2*b*b_i/(b+b_i);
		by=2*b*b_j/(b+b_j);
		bz=2*b*b_k/(b+b_k);

        Id(vx,i,j,k) -= dt*bx*Idx(Oxx,i,j,k)*pmlZone;
		Id(vy,i,j,k) -= dt*by*Idx(Oyx,i,j,k)*pmlZone;
        Id(vz,i,j,k) -= dt*bz*Idx(Ozx,i,j,k)*pmlZone;

        Id(sigmaxx,i,j,k) -= dt*n1_xx*Idx(Pxx,i,j,k)*pmlZone;
		Id(sigmayy,i,j,k) -= dt*n1_yy*Idx(Pxx,i,j,k)*pmlZone;
        Id(sigmazz,i,j,k) -= dt*n1_zz*Idx(Pxx,i,j,k)*pmlZone;
        Id(sigmaxy,i,j,k) -= dt*mu_xy*Idx(Pyx,i,j,k)*pmlZone;
		Id(sigmazx,i,j,k) -= dt*mu_zx*Idx(Pzx,i,j,k)*pmlZone;
    }
}


__global__ void pmlx_back(float *velp,
                        float *vels,
                        float *rho,
                        float *Oxx, 
                        float *Oyx,
						float *Ozx, 
                        float *Pxx, 
                        float *Pyx,
						float *Pzx, 
						float *sigmaxx,
                        float *sigmayy,
						float *sigmazz,
                        float *sigmaxy,
						float *sigmazx,
						float *vx,
                        float *vy,
                        float *vz,
                        int *top,
                        float dt, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i1 = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
    float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_xy, mu_zx;
	int i;
    float n1_xx, n1_yy, n1_zz;
    float b, b_i, b_j, b_k, bx, by, bz;
	float eps, eps_i, eps_j, eps_k, eps_ij, eps_ik;
	float pmlZone;
    if ( i1>1 && i1 < nb && j>1 && j < ny-2 && k>1  && k < nz-2){
		i=nx-1-i1;
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
      	lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;
		eps = (Id(vels,i,j,k)==0);
		eps_i =  float(Id(vels,i+1,j,k)==0);
		eps_j =  float(Id(vels,i,j+1,k)==0);
		eps_k =  float(Id(vels,i,j,k+1)==0) ;
		eps_ij = float(Id(vels,i+1,j+1,k)==0);
		eps_ik = float(Id(vels,i+1,j,k+1)==0) ;
		pmlZone = float(Id(vels,i,j,k-2)>0);
	   	mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k)+eps_i;
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k)+eps_j;
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1)+eps_k;
		mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k)+eps_ij;
		mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1)+eps_ik;
		mu_xy = 1/(0.25/(mu+eps)+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
		mu_zx = 1/(0.25/(mu+eps)+0.25/mu_i+0.25/mu_k+0.25/mu_ik);

		n1_xx=lambda+2*mu;  
		n1_yy=lambda;     
		n1_zz=lambda;       

		b  = 1/(Id(rho,i,j,k)+eps);
		b_i= 1/(Id(rho,i+1,j,k)+eps_i);
		b_j= 1/(Id(rho,i,j+1,k)+eps_j);
		b_k= 1/(Id(rho,i,j,k+1)+eps_k);
		bx=2*b*b_i/(b+b_i);
		by=2*b*b_j/(b+b_j);
		bz=2*b*b_k/(b+b_k);

        Id(vx,i,j,k) -= dt*bx*Idx(Oxx,2*nb-1-i1,j,k)*pmlZone;
		Id(vy,i,j,k) -= dt*by*Idx(Oyx,2*nb-1-i1,j,k)*pmlZone;
        Id(vz,i,j,k) -= dt*bz*Idx(Ozx,2*nb-1-i1,j,k)*pmlZone;

        Id(sigmaxx,i,j,k) -= dt*n1_xx*Idx(Pxx,2*nb-1-i1,j,k)*pmlZone;
		Id(sigmayy,i,j,k) -= dt*n1_yy*Idx(Pxx,2*nb-1-i1,j,k)*pmlZone;
        Id(sigmazz,i,j,k) -= dt*n1_zz*Idx(Pxx,2*nb-1-i1,j,k)*pmlZone;
        Id(sigmaxy,i,j,k) -= dt*mu_xy*Idx(Pyx,2*nb-1-i1,j,k)*pmlZone;
		Id(sigmazx,i,j,k) -= dt*mu_zx*Idx(Pzx,2*nb-1-i1,j,k)*pmlZone;
    }
}


__global__ void pmly_left(float *velp,
                        float *vels,
                        float *rho,
                        float *Oxy, 
                        float *Oyy,
						float *Ozy, 
                        float *Pxy, 
                        float *Pyy,
						float *Pzy, 
						float *sigmaxx,
                        float *sigmayy,
						float *sigmazz,
                        float *sigmaxy,
						float *sigmayz,
						float *vx,
                        float *vy,
                        float *vz,
                        int *top,
                        float dt, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
    float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_jk, mu_xy, mu_yz;
    float n2_xx, n2_yy, n2_zz;
    float b, b_i, b_j, b_k, bx, by, bz;
	float eps, eps_i, eps_j, eps_k, eps_ij, eps_jk;
	float pmlZone;
    if ( i>1 && i < nx-2 && j>1 && j < nb && k>1 && k < nz-2){
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
      	lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;
		eps = float(Id(vels,i,j,k)==0);
		eps_i =  float(Id(vels,i+1,j,k)==0);
		eps_j =  float(Id(vels,i,j+1,k)==0);
		eps_k =  float(Id(vels,i,j,k+1)==0) ;
		eps_ij = float(Id(vels,i+1,j+1,k)==0);
		eps_jk = float(Id(vels,i,j+1,k+1)==0) ;
		pmlZone = float(Id(vels,i,j,k-2)>0);

	   	mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k)+eps_i;
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k)+eps_j;
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1)+eps_k;
		mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k)+eps_ij;
		mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1)+eps_jk;
		mu_xy = 1/(0.25/(mu+eps)+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
		mu_yz = 1/(0.25/(mu+eps)+0.25/mu_j+0.25/mu_k+0.25/mu_jk);

		n2_xx=lambda;       
		n2_yy=lambda+2*mu;   
		n2_zz=lambda;         

		b  = 1/(Id(rho,i,j,k)+eps);
		b_i= 1/(Id(rho,i+1,j,k)+eps_i);
		b_j= 1/(Id(rho,i,j+1,k)+eps_j);
		b_k= 1/(Id(rho,i,j,k+1)+eps_k);

		bx=2*b*b_i/(b+b_i);
		by=2*b*b_j/(b+b_j);
		bz=2*b*b_k/(b+b_k);

		
        Id(vx,i,j,k) -= dt*bx*Idy(Oxy,i,j,k)*pmlZone;
		Id(vy,i,j,k) -= dt*by*Idy(Oyy,i,j,k)*pmlZone;
        Id(vz,i,j,k) -= dt*bz*Idy(Ozy,i,j,k)*pmlZone;

        Id(sigmaxx,i,j,k) -= dt*n2_xx*Idy(Pyy,i,j,k)*pmlZone;
		Id(sigmayy,i,j,k) -= dt*n2_yy*Idy(Pyy,i,j,k)*pmlZone;
        Id(sigmazz,i,j,k) -= dt*n2_zz*Idy(Pyy,i,j,k)*pmlZone;
        Id(sigmaxy,i,j,k) -= dt*mu_xy*Idy(Pxy,i,j,k)*pmlZone;
		Id(sigmayz,i,j,k) -= dt*mu_yz*Idy(Pzy,i,j,k)*pmlZone;
    }
}

__global__ void pmly_right(float *velp,
                        float *vels,
                        float *rho,
                        float *Oxy, 
                        float *Oyy,
						float *Ozy, 
                        float *Pxy, 
                        float *Pyy,
						float *Pzy, 
						float *sigmaxx,
                        float *sigmayy,
						float *sigmazz,
                        float *sigmaxy,
						float *sigmayz,
						float *vx,
                        float *vy,
                        float *vz,
                        int *top,
                        float dt, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j1 = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
	int j;
    float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_jk, mu_xy, mu_yz;
    float n2_xx, n2_yy, n2_zz;
    float b, b_i, b_j, b_k, bx, by, bz;
	float eps, eps_i, eps_j, eps_k, eps_ij, eps_jk;
	float pmlZone;
    if ( i>1 && i < nx-2 && j1>1 && j1 < nb && k>1 && k < nz-2){
		j = ny-1-j1;
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
      	lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;
		eps = float(Id(vels,i,j,k)==0);
		eps_i =  float(Id(vels,i+1,j,k)==0);
		eps_j =  float(Id(vels,i,j+1,k)==0);
		eps_k =  float(Id(vels,i,j,k+1)==0) ;
		eps_ij = float(Id(vels,i+1,j+1,k)==0);
		eps_jk = float(Id(vels,i,j+1,k+1)==0) ;
		pmlZone = float(Id(vels,i,j,k-2)>0);

	   	mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k)+eps_i;
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k)+eps_j;
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1)+eps_k;
		mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k)+eps_ij;
		mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1)+eps_jk;
		mu_xy = 1/(0.25/(mu+eps)+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
		mu_yz = 1/(0.25/(mu+eps)+0.25/mu_j+0.25/mu_k+0.25/mu_jk);

		n2_xx=lambda;       
		n2_yy=lambda+2*mu;   
		n2_zz=lambda;         

		b  = 1/(Id(rho,i,j,k)+eps);
		b_i= 1/(Id(rho,i+1,j,k)+eps_i);
		b_j= 1/(Id(rho,i,j+1,k)+eps_j);
		b_k= 1/(Id(rho,i,j,k+1)+eps_k);

		bx=2*b*b_i/(b+b_i);
		by=2*b*b_j/(b+b_j);
		bz=2*b*b_k/(b+b_k);

        Id(vx,i,j,k) -= dt*bx*Idy(Oxy,i,2*nb-1-j1,k)*pmlZone;
		Id(vy,i,j,k) -= dt*by*Idy(Oyy,i,2*nb-1-j1,k)*pmlZone;
        Id(vz,i,j,k) -= dt*bz*Idy(Ozy,i,2*nb-1-j1,k)*pmlZone;

        Id(sigmaxx,i,j,k) -= dt*n2_xx*Idy(Pyy,i,2*nb-1-j1,k)*pmlZone;
		Id(sigmayy,i,j,k) -= dt*n2_yy*Idy(Pyy,i,2*nb-1-j1,k)*pmlZone;
        Id(sigmazz,i,j,k) -= dt*n2_zz*Idy(Pyy,i,2*nb-1-j1,k)*pmlZone;
        Id(sigmaxy,i,j,k) -= dt*mu_xy*Idy(Pxy,i,2*nb-1-j1,k)*pmlZone;
		Id(sigmayz,i,j,k) -= dt*mu_yz*Idy(Pzy,i,2*nb-1-j1,k)*pmlZone;
    }
}

__global__ void pmlz_bottom(float *velp,
                        float *vels,
                        float *rho,
                        float *Oxz, 
                        float *Oyz,
						float *Ozz, 
                        float *Pxz, 
                        float *Pyz,
						float *Pzz, 
						float *sigmaxx,
                        float *sigmayy,
						float *sigmazz,
                        float *sigmayz,
						float *sigmazx,
						float *vx,
                        float *vy,
                        float *vz,
                        float dt, 
                        int nb, 
						int nx,
                        int ny,
                        int nz
                        )
{   
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k1 = threadIdx.z + blockIdx.z * blockDim.z;
	int k;
    float lambda, mu, mu_i, mu_j, mu_k, mu_ik, mu_jk, mu_yz, mu_zx;
    float n3_xx, n3_yy, n3_zz;
    float b, b_i, b_j, b_k, bx, by, bz;
    if ( i>1 && i < nx-2 && j>1 && j < ny-2 && k1>1 && k1 < nb){
		k=nz-1-k1;
		mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
      	lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;

	   	mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k);
		mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k);
		mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1);
		mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1);
		mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1);
		mu_yz = 1/(0.25/mu+0.25/mu_j+0.25/mu_k+0.25/mu_jk);
		mu_zx = 1/(0.25/mu+0.25/mu_i+0.25/mu_k+0.25/mu_ik);

		n3_xx=lambda;
		n3_yy=lambda;
		n3_zz=lambda+2*mu;

		b  = 1/Id(rho,i,j,k);
		b_i= 1/Id(rho,i+1,j,k);
		b_j= 1/Id(rho,i,j+1,k);
		b_k= 1/Id(rho,i,j,k+1);

		bx=2*b*b_i/(b+b_i);
		by=2*b*b_j/(b+b_j);
		bz=2*b*b_k/(b+b_k);

        Id(vx,i,j,k) -= dt*bx*Idz(Oxz,i,j,nb-1-k1);
		Id(vy,i,j,k) -= dt*by*Idz(Oyz,i,j,nb-1-k1);
        Id(vz,i,j,k) -= dt*bz*Idz(Ozz,i,j,nb-1-k1);

        Id(sigmaxx,i,j,k) -= dt*n3_xx*Idz(Pzz,i,j,nb-1-k1);
		Id(sigmayy,i,j,k) -= dt*n3_yy*Idz(Pzz,i,j,nb-1-k1);
        Id(sigmazz,i,j,k) -= dt*n3_zz*Idz(Pzz,i,j,nb-1-k1);
        Id(sigmayz,i,j,k) -= dt*mu_yz*Idz(Pyz,i,j,nb-1-k1);
		Id(sigmazx,i,j,k) -= dt*mu_zx*Idz(Pxz,i,j,nb-1-k1);
    }
}


//************* KERNEL PARA EXTRAER GATHER *****************   //
__global__ 
void gather(float *traceV,
			float *v,
			float *rece,
			int *top,
			int *ind_g,
			int cont_g,	
			float idx,
			float idy,
			float idz,
			int nx,
			int ny,
			int nz){
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	int Gx, Gy, Gz;
	if ( ind < cont_g ){
		Gx= (int) (X(rece,ind_g[ind])*idx);
	   Gy= (int) Y(rece,ind_g[ind])*idy;
      Gz= (int) Z(rece,ind_g[ind])*idz + top[(Gx)*ny+Gy];
		traceV[ind] = Id(v,Gx,Gy,Gz);
	}
}

__global__ 
void gatherVx(float *traceVx,
			float *vx,
			float *rece,
			float *vs,
			int *top,
			int *ind_g,
			int cont_g,	
			float idx,
			float idy,
			float idz,
			int nx,
			int ny,
			int nz){
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	float lx,ly,lz,dx,dy,dz;
	float w_tlf,w_tlb,w_trf,w_trb,w_blf,w_blb,w_brf,w_brb;
    int i,j,k;
	float sum;
	float x0 =  1./(2*idx);
	if ( ind < cont_g ){
		lx =  (X(rece,ind_g[ind])-x0)*idx;
	    ly =  (Y(rece,ind_g[ind]))*idy;
		i = int(lx), j = int(ly);
        lz =  (Z(rece,ind_g[ind]))*idz + top[(i)*ny+j];
		k = int(lz);
		if ( (Id(vs,i+2,j,k)==0) || (Id(vs,i+2,j+1,k)==0)  ){
			lz =  (Z(rece,ind_g[ind]))*idz + top[(i+1)*ny+j];
			k = int(lz);
		}

		dx = lx-i, dy = ly-j, dz = lz-k;

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		// w_tlf*= (Id(vs,i,j,k)>0);      
		// w_tlb*= (Id(vs,i+2,j,k)>0);   
		// w_trf*= (Id(vs,i,j+1,k)>0);    
		// w_trb*= (Id(vs,i+2,j+1,k)>0); 
		// w_blf*= (Id(vs,i,j,k+1)>0);    
		// w_blb*= (Id(vs,i+2,j,k+1)>0); 
		// w_brf*= (Id(vs,i,j+1,k+1)>0);  
		// w_brb*= (Id(vs,i+2,j+1,k+1)>0);

		sum = ( w_tlf + w_tlb + w_trf + w_trb + w_blf + w_blb + w_brf + w_brb );
		traceVx[ind] =  (1./sum)*(w_tlf*Id(vx,i,j,k)     +  w_tlb*Id(vx,i+1,j,k)    +
								  w_trf*Id(vx,i,j+1,k)   +  w_trb*Id(vx,i+1,j+1,k)  +
								  w_blf*Id(vx,i,j,k+1)   +  w_blb*Id(vx,i+1,j,k+1)  +
								  w_brf*Id(vx,i,j+1,k+1) +  w_brb*Id(vx,i+1,j+1,k+1));
	}
}

__global__ 
void gatherVy(float *traceVy,
			float *vy,
			float *rece,
			float *vs,
			int *top,
			int *ind_g,
			int cont_g,	
			float idx,
			float idy,
			float idz,
			int nx,
			int ny,
			int nz){
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	float lx,ly,lz,dx,dy,dz;
	float w_tlf,w_tlb,w_trf,w_trb,w_blf,w_blb,w_brf,w_brb;
    int i,j,k;
	float sum;
	float y0 =  1./(2*idy);
	if ( ind < cont_g ){
		lx =  (X(rece,ind_g[ind]))*idx;
	    ly =  (Y(rece,ind_g[ind])-y0)*idy;
		i = int(lx), j = int(ly);
        lz =  (Z(rece,ind_g[ind]))*idz + top[(i)*ny+j];
		k = int(lz);
		if ( (Id(vs,i,j+2,k)==0) || (Id(vs,i+1,j+2,k)==0) ){
			lz =  (Z(rece,ind_g[ind]))*idz + top[(i)*ny+j+1];
			k = int(lz);
		}

		dx = lx-i, dy = ly-j, dz = lz-k;

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		// w_tlf*= (Id(vs,i,j,k)>0);      
		// w_tlb*= (Id(vs,i+1,j,k)>0);   
		// w_trf*= (Id(vs,i,j+2,k)>0);    
		// w_trb*= (Id(vs,i+1,j+2,k)>0); 
		// w_blf*= (Id(vs,i,j,k+1)>0);    
		// w_blb*= (Id(vs,i+1,j,k+1)>0); 
		// w_brf*= (Id(vs,i,j+2,k+1)>0);  
		// w_brb*= (Id(vs,i+1,j+2,k+1)>0);

		sum = ( w_tlf + w_tlb + w_trf + w_trb + w_blf + w_blb + w_brf + w_brb );
		traceVy[ind] =  (1./sum)*(w_tlf*Id(vy,i,j,k)     +  w_tlb*Id(vy,i+1,j,k)    +
								  w_trf*Id(vy,i,j+1,k)   +  w_trb*Id(vy,i+1,j+1,k)  +
								  w_blf*Id(vy,i,j,k+1)   +  w_blb*Id(vy,i+1,j,k+1)  +
								  w_brf*Id(vy,i,j+1,k+1) +  w_brb*Id(vy,i+1,j+1,k+1));
	}
}


__global__ 
void gatherVz(float *traceVz,
			float *vz,
			float *rece,
			float *vs,
			int *top,
			int *ind_g,
			int cont_g,	
			float idx,
			float idy,
			float idz,
			int nx,
			int ny,
			int nz){
	int ind = threadIdx.x + blockIdx.x*blockDim.x;
	float lx,ly,lz,dx,dy,dz;
	float w_tlf,w_tlb,w_trf,w_trb,w_blf,w_blb,w_brf,w_brb;
    int i,j,k;
	float z0 =  1./(2*idz);
	float sum;
	if ( ind < cont_g ){
		lx =  (X(rece,ind_g[ind]))*idx;
	    ly =  (Y(rece,ind_g[ind]))*idy;
		i = int(lx), j = int(ly);
        lz =  (Z(rece,ind_g[ind])-z0)*idz + top[(i)*ny+j];
		k = int(lz);
		dx = lx-i, dy = ly-j, dz = lz-k;

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		// w_tlf*= (Id(vs,i,j,k)>0);      
		// w_tlb*= (Id(vs,i+1,j,k)>0);   
		// w_trf*= (Id(vs,i,j+1,k)>0);    
		// w_trb*= (Id(vs,i+1,j+1,k)>0); 
		// w_blf*= (Id(vs,i,j,k+1)>0);    
		// w_blb*= (Id(vs,i+1,j,k+1)>0); 
		// w_brf*= (Id(vs,i,j+1,k+1)>0);  
		// w_brb*= (Id(vs,i+1,j+1,k+1)>0);

		sum = ( w_tlf + w_tlb + w_trf + w_trb + w_blf + w_blb + w_brf + w_brb );
		traceVz[ind] =  (1./sum)*(w_tlf*Id(vz,i,j,k)     +  w_tlb*Id(vz,i+1,j,k)    +
								  w_trf*Id(vz,i,j+1,k)   +  w_trb*Id(vz,i+1,j+1,k)  +
								  w_blf*Id(vz,i,j,k+1)   +  w_blb*Id(vz,i+1,j,k+1)  +
								  w_brf*Id(vz,i,j+1,k+1) +  w_brb*Id(vz,i+1,j+1,k+1));
	}
}
