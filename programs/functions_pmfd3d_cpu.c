/*
 ============================================================================
 Name        : functions_pmfd3d_cpu.c
 Author      : Ivan Sánchez
 Version     :
 Copyright   :
 Description : Functions host.
 ============================================================================
 */
#include<stdlib.h>
#include <stdbool.h>
#define Id(A,i,j,k) (A)[ (i)*ny*nz + (j)*nz + (k) ]

#define X(a,i) (a)[3*(i)]
#define Y(a,i) (a)[3*(i)+1]
#define Z(a,i) (a)[3*(i)+2]

#define STENCIL_WIDTH 2
/*   FD Coefficients */
#define C1 9.0f/8.0f
#define C2 -1.0f/24.0f
/* backward  FD derivative stencils */
#define Dxb(A,i,j,k,s) (C2*( (A)[(i+1)*ny*nz+(j)*nz+(k)] - (A)[(i-2)*ny*nz+(j)*nz+(k)] ) +	\
  	     C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i-1)*ny*nz+(j)*nz+(k)] ) )*s
#define Dyb(A,i,j,k,s) (C2*( (A)[(i)*ny*nz+(j+1)*nz+(k)] - (A)[(i)*ny*nz+(j-2)*nz+(k)] ) +	\
  		C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i)*ny*nz+(j-1)*nz+(k)] ) )*s
#define Dzb(A,i,j,k,s) (C2*( (A)[(i)*ny*nz+(j)*nz+(k+1)] - (A)[(i)*ny*nz+(j)*nz+(k-2)] ) +	\
  		C1*( (A)[(i)*ny*nz+(j)*nz+(k)]   - (A)[(i)*ny*nz+(j)*nz+(k-1)] ) )*s

/* forward FD derivative stencils */
#define Dxf(A,i,j,k,s) (C2*((A)[(i+2)*ny*nz+(j)*nz+(k)] - (A)[(i-1)*ny*nz+(j)*nz+(k)] )+	\
  		C1*((A)[(i+1)*ny*nz+(j)*nz+(k)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s
#define Dyf(A,i,j,k,s) (C2*((A)[(i)*ny*nz+(j+2)*nz+(k)] - (A)[(i)*ny*nz+(j-1)*nz+(k)] ) +	\
  		C1*((A)[(i)*ny*nz+(j+1)*nz+(k)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s
#define Dzf(A,i,j,k,s) (C2*((A)[(i)*ny*nz+(j)*nz+(k+2)] - (A)[(i)*ny*nz+(j)*nz+(k-1)] ) +	\
  		C1*((A)[(i)*ny*nz+(j)*nz+(k+1)] - (A)[(i)*ny*nz+(j)*nz+(k)])  )*s

/* Funcion de tiempo*/
double time_measure(){
	struct timeval tp;
	gettimeofday(&tp,NULL);
	return ((double)tp.tv_sec +(double)tp.tv_usec*1.e-6);
}

/*******************************************/
/* Expand model*/
/*******************************************/

void expand(float*vv, float *v0, int nz, int nx, int ny, int nz1, int nx1, int ny1)
{
    int i,j,k,i1,j1,k1,nb;
    nb = (nz - nz1);
    for(i=0; i<nx; i++){
	    for(j=0; j<ny; j++){
            for(k=0; k<nz; k++)
	        {
                k1=(k<nz1)?k:(nz1-1);
                i1=(i<nb)?0:((i>=(nx1+nb))?(nx1-1):i-nb);
                j1=(j<nb)?0:((j>=(ny1+nb))?(ny1-1):j-nb);
                Id(vv,i,j,k) = v0[(i1)*ny1*nz1+(j1)*nz1+(k1)];
	        }
        }
    }
}

/*******************************************/
/* Window model*/
/*******************************************/


void window(float*v0, float *vv, int nz1, int nx1, int ny1, int nz, int nx, int ny)
{
    int i,j,k,i1,j1,k1,nb;
    nb = (nz - nz1);
    for(i1=0; i1<nx1; i1++){
	    for(j1=0; j1<ny1; j1++){
            for(k1=0; k1<nz1; k1++)
	        {
                k=k1;
                i=i1+nb; 
                j=j1+nb; 
                v0[(i1)*ny1*nz1+(j1)*nz1+(k1)] = Id(vv,i,j,k);
	        }
        }
    }
}


/*******************************************/
/* Check receivers and sources position*/
/*******************************************/
bool check_pos(float *s, int ns, float ox, float oy, float oz, float dx, float dy, float dz, int nx, int ny, int nz){
    int i;
    for (i=0; i<ns; i++){
        if ((X(s,i) <  ox) || (X(s,i) >  ox+dx*(nx-1)) ) return true; 
        if ((Y(s,i) <  oy) || (Y(s,i) >  oy+dy*(ny-1)) ) return true;  
        if ((Z(s,i) <  oz) || (Z(s,i) >  oz+dz*(nz-1)) ) return true;  
    } 
    return false;
}

/*******************************************/
/* Update position*/
/*******************************************/
void update_pos(float *s, int ns, int nb, float dx,  float dy){
    int i;
    for (i=0; i<ns; i++){
        X(s,i) +=  dx*nb ;
        Y(s,i) +=  dy*nb ;
    } 
}


/*******************************************/
/* Compute ABC*/
/*******************************************/

void init_abc(int bound,
              float vmax,
              float R,
              float dt,
              float dh,
              float *a)
{
    float L=bound*dh;
    float d0 = -4*vmax*logf(R)/L;
    float sigma;
    for(int i=0; i<bound; i++){
        //sigma = d0*pow((bound-i)*dh/L,4);
        //a[i] = exp(-sigma*dt);
        sigma = pow(0.01*(bound-i),2);
        a[i] = exp(-sigma);
    }
}

/*******************************************/
/* Compute topography*/
/*******************************************/
void topography (float *vs, 
                 int *top, 
                 int nx, 
                 int ny, 
                 int nz){
     int i, j, k;
     bool H, HT, VRT, VLT, VFT, VBT, OR, OL, OF, OB, IR, IB;
     for (i=1; i<nx-1; i++){
      	 for (j=1; j<ny-1; j++){
            for (k=1; k<nz-1; k++){
                // if (Id(vs,i,j,k)!=0 &&   Id(vs,i,j,k-1) ==0    && ( ( Id(vs,i-1,j,k)+Id(vs,i+1,j,k)==0 )   ||  ( Id(vs,i,j-1,k)+Id(vs,i,j+1,k)==0 ) ) )
                //     Id(vs,i,j,k)=0;
                HT  = ( Id(vs,i,j,k)!=0 && Id(vs,i,j,k-1) ==0 );
                VRT = ( Id(vs,i,j,k)!=0 && Id(vs,i+1,j,k) ==0 );
                VLT = ( Id(vs,i,j,k)!=0 && Id(vs,i-1,j,k) ==0 );
                VFT = ( Id(vs,i,j,k)!=0 && Id(vs,i,j-1,k) ==0 );
                VBT = ( Id(vs,i,j,k)!=0 && Id(vs,i,j+1,k) ==0 );
                OR = ( HT && VRT );
                OL = ( HT && VLT );
                OB = ( HT && VBT );
                OF = ( HT && VFT );
                IR = ( Id(vs,i+1,j,k-1)==0 && !HT && !VRT);
                IB = ( Id(vs,i,j+1,k-1)==0 && !HT && !VBT);
                H  = ( HT && !OR && !OL && !OF && !OB );
                if ( H || OL || OF || IR || IB ) {top[(i)*ny+j]=k;}
            }
         }
     }

    for (i=0; i<nx; i++)
        top[(i)*ny]=top[(i)*ny+1];
    for (j=0; j<ny; j++)
        top[j]=top[ny+j];
    for (j=0; j<ny; j++)
        top[(nx-1)*ny+j]=top[(nx-2)*ny+j];
}

int top_max (float *rho, 
             int nx, 
             int ny, 
             int nz){
     int i, j, k, nz_ns;
     nz_ns=0;
     for (i=0; i<nx; i++){
      	 for (j=0; j<ny; j++){
             for (k=1; k<nz; k++){
                if (rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz+(j)*nz+(k-1)] ==0 && nz_ns < k+1) {nz_ns=k+1;}
            }
         }
     }
     return nz_ns;
}


/*******************************************/
/* Compute number of points in surface
   and interior regions*/
/*******************************************/
void indices_lengths(float *rho, 
                     int nx, 
                     int ny, 
                     int nz, 
                     int *cont_s, 
                     int *cont_i){
    int i, j, k;
		*cont_s=0;
		*cont_i=0;
		bool H, VR, VL, VF, VB, TP;

        for (i=2; i<nx-2; i++){
            for (j=2; j<ny-2; j++){
                for (k=2; k<nz-2; k++){
    				H  = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)] ==0 );
    				VR = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i+1)*ny*nz + (j)*nz + (k)] ==0 );
    				VL = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i-1)*ny*nz + (j)*nz + (k)] ==0 );
    				VF = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j-1)*nz + (k)] ==0 );
    				VB = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j+1)*nz + (k)] ==0 );
    				TP = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)]*rho[(i-1)*ny*nz + (j)*nz + (k-1)]*rho[(i+1)*ny*nz + (j)*nz + (k-1)]*rho[(i)*ny*nz + (j-1)*nz + (k-1)]*rho[(i)*ny*nz + (j+1)*nz + (k-1)] ==0 );
                    if ( H || VR || VL || VF || VB || TP) {
                        *cont_s+=1;
                    }
    				if ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && !( H || VR || VL || VF || VB || TP )) {
                        *cont_i+=1;
                    }
                }
            }
        }
}

/*******************************************/
/* Compute indices arrays for surface
   and interior regions*/
/*******************************************/
void indices_arrays(float *rho, 
                    int nx, 
                    int ny, 
                    int nz, 
                    int *ind_s, 
                    int *ind_i){
    int i, j, k;
		int cont_s=0;
		int cont_i=0;
		bool H, VR, VL, VF, VB, TP;
        for (i=2; i<nx-2; i++){
          	for (j=2; j<ny-2; j++){
                 for (k=2; k<nz-2; k++){
                    H  = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)] ==0 );
                    VR = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i+1)*ny*nz + (j)*nz + (k)] ==0 );
                    VL = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i-1)*ny*nz + (j)*nz + (k)] ==0 );
                    VF = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j-1)*nz + (k)] ==0 );
                    VB = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j+1)*nz + (k)] ==0 );
                    TP = ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && rho[(i)*ny*nz + (j)*nz + (k-1)]*rho[(i-1)*ny*nz + (j)*nz + (k-1)]*rho[(i+1)*ny*nz + (j)*nz + (k-1)]*rho[(i)*ny*nz + (j-1)*nz + (k-1)]*rho[(i)*ny*nz + (j+1)*nz + (k-1)] ==0 );

                    if ( H || VR || VL || VF || VB || TP) {
                        ind_s[cont_s]= (i)*ny*nz + (j)*nz + (k);
                        cont_s+=1;
                    }
                    if ( rho[(i)*ny*nz+(j)*nz+(k)]!=0 && !( H || VR || VL || VF || VB || TP )) {
                        ind_i[cont_i]= (i)*ny*nz + (j)*nz + (k);
                        cont_i+=1;
                    }
                }
            }
        }
}

/*******************************************/
/* Compute indices arrays for receivers*/
/*******************************************/

void indices_length_rec(int *cont_g, 
                        float *rece, 
                        int ng, 
                        int nx, 
                        float idx){
    int g;
    int Gx;
    *cont_g=0;
    for (g=0; g<ng; g++){
        Gx= (int) (X(rece,g)*idx);
        if (Gx >= STENCIL_WIDTH && Gx < nx-STENCIL_WIDTH){
            *cont_g+=1;
        }
    }
}

void indices_array_rec(int *ind_g, 
                      float *rece, 
                      int *top, 
                      int ng, 
                      int nx, 
                      float idx){
    int g;
    int Gx;
    int cont_g=0;

    for (g=0; g<ng; g++){
        Gx= (int) (X(rece,g)*idx);
        if (Gx >= STENCIL_WIDTH && Gx < nx-STENCIL_WIDTH){
            ind_g[cont_g]=g;
            cont_g+=1;
        }
    }
}


/*******************************************/
/* Compute parameters in the surface region*/
/*******************************************/
void parameter_fs(float *velp,
                  float *vels,
                  float *rho,
                  float *bxs,
                  float *bys,
                  float *bzs,
                  float *mu_xys,
                  float *mu_yzs,
                  float *mu_zxs,
                  float *n1_xxs,
                  float *n2_xxs,
                  float *n3_xxs,
                  float *n1_yys,
                  float *n2_yys,
                  float *n3_yys,
                  float *n1_zzs,
                  float *n2_zzs,
                  float *n3_zzs,
                  int *ind_s,
                  int cont_s,
                  int nx,
                  int ny,
                  int nz
                  )
{
     int i, j, k, ind;
     float b, b_i, b_j, b_k, bx, by, bz;
     float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_jk, mu_xy, mu_yz, mu_zx;
     float n1_xx, n2_xx, n3_xx, n1_yy, n2_yy, n3_yy, n1_zz, n2_zz, n3_zz;
     bool H, VR, VL, VF, VB, TP, IBR, IB, IR, OP;// IL, IF;

     for (ind=0; ind<cont_s; ind++){
          i= ind_s[ind]/(ny*nz);
		j= (ind_s[ind]-(i)*(ny*nz))/nz;
      	k= ind_s[ind]- ((i)*ny*nz + (j)*nz);
          mu=Id(vels,i,j,k)*Id(vels,i,j,k)*Id(rho,i,j,k);
          lambda = Id(velp,i,j,k)*Id(velp,i,j,k)*Id(rho,i,j,k)-2*mu;
          if (Id(rho,i,j,k)!=0){b = 1/Id(rho,i,j,k);}  else{b = 0;}
          if (Id(rho,i+1,j,k)!=0){ b_i= 1/Id(rho,i+1,j,k); bx=2*b*b_i/(b+b_i);}  else{bx =0;}
          if (Id(rho,i,j+1,k)!=0){b_j= 1/Id(rho,i,j+1,k);  by=2*b*b_j/(b+b_j);}  else{by = 0;}
          if (Id(rho,i,j,k+1)!=0){b_k= 1/Id(rho,i,j,k+1);  bz=2*b*b_k/(b+b_k);}  else{bz = 0;}

          mu_i = Id(vels,i+1,j,k)*Id(vels,i+1,j,k)*Id(rho,i+1,j,k);
          mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k);
          mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1);
          mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k);
          mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1);
          mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1);

          if (mu!=0 && mu_i!=0 && mu_j!=0 && mu_ij!=0){ mu_xy = 1/(0.25/mu+0.25/mu_i+0.25/mu_j+0.25/mu_ij);}
          else{ mu_xy = 0;}
          if (mu!=0 && mu_j!=0 && mu_k!=0 && mu_jk!=0){ mu_yz = 1/(0.25/mu+0.25/mu_j+0.25/mu_k+0.25/mu_jk);}
          else{mu_yz = 0;}
          if (mu!=0 && mu_i!=0 && mu_k!=0 && mu_ik!=0){ mu_zx = 1/(0.25/mu+0.25/mu_i+0.25/mu_k+0.25/mu_ik);}
          else{ mu_zx = 0;}


          if (Id(rho,i,j,k)!=0 &&   Id(rho,i,j,k-1) ==0    && ( ( Id(rho,i-1,j,k)+Id(rho,i+1,j,k)==0 )   ||  ( Id(rho,i,j-1,k)+Id(rho,i,j+1,k)==0 ) ) ){
               Id(rho,i,j,k)=0;
               mu=0;
               lambda=0;
            //    mu_xy = 0;
            //    mu_yz = 0;
            //    mu_zx = 0;
          }

          n1_xx=lambda+2*mu;  n2_xx=lambda;         n3_xx=lambda;
          n1_yy=lambda;       n2_yy=lambda+2*mu;    n3_yy=lambda;
          n1_zz=lambda;       n2_zz=lambda;         n3_zz=lambda+2*mu;

          bxs[ind]=bx; bys[ind]=by; bzs[ind]=bz;
          mu_xys[ind]=mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
          n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
          n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
          n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;

          H  = ( Id(rho,i,j,k)!=0 && Id(rho,i,j,k-1) ==0 );
          VR = ( Id(rho,i,j,k)!=0 && Id(rho,i+1,j,k) ==0 );
          VL = ( Id(rho,i,j,k)!=0 && Id(rho,i-1,j,k) ==0 );
          VF = ( Id(rho,i,j,k)!=0 && Id(rho,i,j-1,k) ==0 );
          VB = ( Id(rho,i,j,k)!=0 && Id(rho,i,j+1,k) ==0 );
          TP = ( Id(rho,i,j,k)!=0 && Id(rho,i,j,k-1)*Id(rho,i-1,j,k-1)*Id(rho,i+1,j,k-1)*Id(rho,i,j-1,k-1)*Id(rho,i,j+1,k-1) ==0 );
          OP = ( (H && (VR || VL || VF || VB) ) || (VR && (VF || VB) ) || ( VL && (VF || VB) ) || (VL && VR) || (VF && VB) );
          IR = ( TP && !H && !OP && !VR && Id(rho,i+1,j,k-1)==0 );
          IB = ( TP && !H && !OP && !VB && Id(rho,i,j+1,k-1)==0 );
          IBR = ( IR && IB );
//                 IL = ( TP && !H && !OP && !VL && Id(rho,i-1,j,k-1)==0 );
//                 IF = ( TP && !H && !OP && !VF && Id(rho,i,j-1,k-1)==0 );

          if ( H && !OP ) {
               bxs[ind]=2*bx; bys[ind]=2*by; bzs[ind]=bz;
               mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
               n1_xxs[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n2_xxs[ind]=mu*lambda/(lambda+2*mu); n3_xxs[ind]=0;
               n1_yys[ind]=mu*lambda/(lambda+2*mu); n2_yys[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n3_yys[ind]=0;
               n1_zzs[ind]=0; n2_zzs[ind]=0; n3_zzs[ind]=0;
          }

          if ( VR && !( H || VL || VF || VB) ) {
               bxs[ind]=bx; bys[ind]=2*by; bzs[ind]=2*bz;
               mu_xys[ind]=mu_xy; mu_yzs[ind]=0.5*mu_yz; mu_zxs[ind]=mu_zx;
               n1_xxs[ind]=0; n2_xxs[ind]=0; n3_xxs[ind]=0;
             	n1_yys[ind]=0; n2_yys[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n3_yys[ind]=mu*lambda/(lambda+2*mu);
             	n1_zzs[ind]=0; n2_zzs[ind]=mu*lambda/(lambda+2*mu); n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

  	     if ( VL && !( H || VR || VF || VB) ) {
               bxs[ind]=bx; bys[ind]=2*by; bzs[ind]=2*bz;
             	mu_xys[ind]=mu_xy; mu_yzs[ind]=0.5*mu_yz; mu_zxs[ind]=mu_zx;
              	n1_xxs[ind]=0; n2_xxs[ind]=0; n3_xxs[ind]=0;
              	n1_yys[ind]=0; n2_yys[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n3_yys[ind]=mu*lambda/(lambda+2*mu);
        	     n1_zzs[ind]=0; n2_zzs[ind]=mu*lambda/(lambda+2*mu); n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

          if ( VF && !( H || VL || VR || VB) ) {
               bxs[ind]=2*bx; bys[ind]=by; bzs[ind]=2*bz;
              	mu_xys[ind]=mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=0.5*mu_zx;
              	n1_xxs[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n2_xxs[ind]=0; n3_xxs[ind]=mu*lambda/(lambda+2*mu);
              	n1_yys[ind]=0; n2_yys[ind]=0; n3_yys[ind]=0;
              	n1_zzs[ind]=mu*lambda/(lambda+2*mu); n2_zzs[ind]=0; n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

          if (  VB && !( H || VL || VR || VF) ) {
               bxs[ind]=2*bx; bys[ind]=by; bzs[ind]=2*bz;
              	mu_xys[ind]=mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=0.5*mu_zx;
              	n1_xxs[ind]=2*mu*(lambda+mu)/(lambda+2*mu); n2_xxs[ind]=0; n3_xxs[ind]=mu*lambda/(lambda+2*mu);
              	n1_yys[ind]=0; n2_yys[ind]=0; n3_yys[ind]=0;
        	     n1_zzs[ind]=mu*lambda/(lambda+2*mu); n2_zzs[ind]=0; n3_zzs[ind]=2*mu*(lambda+mu)/(lambda+2*mu);
          }

          if ( OP ) {
               bxs[ind]=2*bx; bys[ind]=2*by; bzs[ind]=2*bz;
              	mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=0.5*mu_yz; mu_zxs[ind]=0.5*mu_zx;
        	     n1_xxs[ind]=0; n2_xxs[ind]=0; n3_xxs[ind]=0;
              	n1_yys[ind]=0; n2_yys[ind]=0; n3_yys[ind]=0;
              	n1_zzs[ind]=0; n2_zzs[ind]=0; n3_zzs[ind]=0;
          }

          if (  IBR ) {
               bxs[ind]=bx; bys[ind]=by; bzs[ind]=bz;
              	mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
              	n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
              	n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
        	     n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;
          }

        	if ( IR && !IBR ) {
               bxs[ind]=2*bx; bys[ind]=by; bzs[ind]=bz;
        	     mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
              	n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
        	     n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
        	     n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;
          }

        	if ( IB && !IBR ) {
               bxs[ind]=bx; bys[ind]=2*by; bzs[ind]=bz;
              	mu_xys[ind]=0.5*mu_xy; mu_yzs[ind]=mu_yz; mu_zxs[ind]=mu_zx;
        	     n1_xxs[ind]=n1_xx; n2_xxs[ind]=n2_xx; n3_xxs[ind]=n3_xx;
              	n1_yys[ind]=n1_yy; n2_yys[ind]=n2_yy; n3_yys[ind]=n3_yy;
        	     n1_zzs[ind]=n1_zz; n2_zzs[ind]=n2_zz; n3_zzs[ind]=n3_zz;
          }

    }
}

#include <omp.h>

void sigma_surface(float *vx, 
                   float *vy, 
                   float *vz, 
                   float *sigmaxx, 
                   float *sigmayy, 
                   float *sigmazz, 
                   float *sigmaxy, 
                   float *sigmayz, 
                   float *sigmazx, 
                   float *mu_xys, 
                   float *mu_yzs, 
                   float *mu_zxs, 
                   float *n1_xxs, 
                   float *n2_xxs, 
                   float *n3_xxs, 
                   float *n1_yys, 
                   float *n2_yys, 
                   float *n3_yys, 
                   float *n1_zzs, 
                   float *n2_zzs, 
                   float *n3_zzs, 
                   int *ind_s, 
                   int cont_s, 
                   float dt, 
                   float idx, 
                   float idy, 
                   float idz, 
                   int nx, 
                   int ny, 
                   int nz) 
{
    int i, j, k;

    #pragma omp parallel for private(i, j, k)
    for (int ind = 0; ind < cont_s; ind++) {
        i = ind_s[ind] / (ny * nz);
        j = (ind_s[ind] - (i) * (ny * nz)) / nz;
        k = ind_s[ind] - ((i) * ny * nz + (j) * nz);
        Id(sigmaxx,i,j,k) += (dt)*(n1_xxs[ind]*Dxb(vx,i,j,k,idx) + n2_xxs[ind]*Dyb(vy,i,j,k,idy) + n3_xxs[ind]*Dzb(vz,i,j,k,idz));
        Id(sigmayy,i,j,k) += (dt)*(n1_yys[ind]*Dxb(vx,i,j,k,idx) + n2_yys[ind]*Dyb(vy,i,j,k,idy) + n3_yys[ind]*Dzb(vz,i,j,k,idz));
        Id(sigmazz,i,j,k) += (dt)*(n1_zzs[ind]*Dxb(vx,i,j,k,idx) + n2_zzs[ind]*Dyb(vy,i,j,k,idy) + n3_zzs[ind]*Dzb(vz,i,j,k,idz));
        Id(sigmaxy,i,j,k) += (dt)*mu_xys[ind]*(Dxf(vy,i,j,k,idx) + Dyf(vx,i,j,k,idy));
        Id(sigmayz,i,j,k) += (dt)*mu_yzs[ind]*(Dyf(vz,i,j,k,idy) + Dzf(vy,i,j,k,idz));
        Id(sigmazx,i,j,k) += (dt)*mu_zxs[ind]*(Dxf(vz,i,j,k,idx) + Dzf(vx,i,j,k,idz));
    }
}

void sigma_inside(float *velp,
                  float *vels,
                  float *rho,
                  float *vx, 
                  float *vy, 
                  float *vz, 
                  float *sigmaxx, 
                  float *sigmayy, 
                  float *sigmazz, 
                  float *sigmaxy, 
                  float *sigmayz, 
                  float *sigmazx, 
                  int *ind_i, 
                  int cont_i, 
                  float dt, 
                  float idx, 
                  float idy, 
                  float idz, 
                  int nx, 
                  int ny, 
                  int nz) 
{
    int i, j, k;
    float lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_jk, mu_xy, mu_yz, mu_zx;
    float n1_xx, n2_xx, n3_xx, n1_yy, n2_yy, n3_yy, n1_zz, n2_zz, n3_zz;

    #pragma omp parallel for private(i, j, k, lambda, mu, mu_i, mu_j, mu_k, mu_ij, mu_ik, mu_jk, mu_xy, mu_yz, mu_zx, n1_xx, n2_xx, n3_xx, n1_yy, n2_yy, n3_yy, n1_zz, n2_zz, n3_zz)
    for (int ind = 0; ind < cont_i; ind++) {
        i = ind_i[ind] / (ny * nz);
        j = (ind_i[ind] - (i) * (ny * nz)) / nz;
        k = ind_i[ind] - ((i) * ny * nz + (j) * nz);
        
        mu = Id(vels,i,j,k) * Id(vels,i,j,k) * Id(rho,i,j,k);
        lambda = Id(velp,i,j,k) * Id(velp,i,j,k) * Id(rho,i,j,k) - 2*mu;
        mu_i = Id(vels,i+1,j,k) * Id(vels,i+1,j,k) * Id(rho,i+1,j,k);
        mu_j = Id(vels,i,j+1,k)*Id(vels,i,j+1,k)*Id(rho,i,j+1,k);
        mu_k = Id(vels,i,j,k+1)*Id(vels,i,j,k+1)*Id(rho,i,j,k+1);
        mu_ij = Id(vels,i+1,j+1,k)*Id(vels,i+1,j+1,k)*Id(rho,i+1,j+1,k);
        mu_ik = Id(vels,i+1,j,k+1)*Id(vels,i+1,j,k+1)*Id(rho,i+1,j,k+1);
        mu_jk = Id(vels,i,j+1,k+1)*Id(vels,i,j+1,k+1)*Id(rho,i,j+1,k+1);
        mu_xy = 1/(0.25/mu+0.25/mu_i+0.25/mu_j+0.25/mu_ij);
        mu_yz = 1/(0.25/mu+0.25/mu_j+0.25/mu_k+0.25/mu_jk);
        mu_zx = 1/(0.25/mu+0.25/mu_i+0.25/mu_k+0.25/mu_ik);

        n1_xx=lambda+2*mu;  n2_xx=lambda;         n3_xx=lambda;
        n1_yy=lambda;       n2_yy=lambda+2*mu;    n3_yy=lambda;
        n1_zz=lambda;       n2_zz=lambda;         n3_zz=lambda+2*mu;

        Id(sigmaxx,i,j,k) += (dt)*(n1_xx*Dxb(vx,i,j,k,idx) + n2_xx*Dyb(vy,i,j,k,idy) + n3_xx*Dzb(vz,i,j,k,idz));
        Id(sigmayy,i,j,k) += (dt)*(n1_yy*Dxb(vx,i,j,k,idx) + n2_yy*Dyb(vy,i,j,k,idy) + n3_yy*Dzb(vz,i,j,k,idz));
        Id(sigmazz,i,j,k) += (dt)*(n1_zz*Dxb(vx,i,j,k,idx) + n2_zz*Dyb(vy,i,j,k,idy) + n3_zz*Dzb(vz,i,j,k,idz));
        Id(sigmaxy,i,j,k) += (dt)*mu_xy*(Dxf(vy,i,j,k,idx) + Dyf(vx,i,j,k,idy));
        Id(sigmayz,i,j,k) += (dt)*mu_yz*(Dyf(vz,i,j,k,idy) + Dzf(vy,i,j,k,idz));
        Id(sigmazx,i,j,k) += (dt)*mu_zx*(Dxf(vz,i,j,k,idx) + Dzf(vx,i,j,k,idz));
    }
}

void source_insert(float source_it, 
                   float *sigmaxx,  float *sigmayy, float *sigmazz,
                   int *top, 
                   float idx,       float idy,      float idz, 
                   int nx,          int ny,         int nz, 
                   float Sx,        float Sy,       float Sz)
{
    float lx,ly,lz,dx,dy,dz;
    float w_tlf,w_tlb,w_trf,w_trb,w_blf,w_blb,w_brf,w_brb;
    int Ix,Iy,Iz;

    lx = (Sx)*idx;
    ly = (Sy)*idy;
    Ix = (int)lx; 
    Iy = (int)ly;
    lz = (Sz)*idz + top[Ix*ny+Iy];
    Iz = (int)lz;
    dx = lx-Ix, dy = ly-Iy, dz = lz-Iz;

    w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
    w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
    w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
    w_trb= (1-dz)*dy*dx;           // top-right-back area
    w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
    w_blb= dz*(1-dy)*dx;           // bottom-left-back area
    w_brf= dz*dy*(1-dx);           // bottom-right-front area
    w_brb= dz*dy*dx;               // bottom-right-back area

    Id(sigmaxx,Ix,Iy,Iz) += w_tlf*source_it;      
    Id(sigmaxx,Ix+1,Iy,Iz) += w_tlb*source_it;   
    Id(sigmaxx,Ix,Iy+1,Iz) += w_trf*source_it;      
    Id(sigmaxx,Ix+1,Iy+1,Iz) += w_trb*source_it;    
    Id(sigmaxx,Ix,Iy,Iz+1) += w_blf*source_it;       
    Id(sigmaxx,Ix+1,Iy,Iz+1) += w_blb*source_it;    
    Id(sigmaxx,Ix,Iy+1,Iz+1) += w_brf*source_it;     
    Id(sigmaxx,Ix+1,Iy+1,Iz+1) += w_brb*source_it;   

    Id(sigmayy,Ix,Iy,Iz) += w_tlf*source_it;         
    Id(sigmayy,Ix+1,Iy,Iz) += w_tlb*source_it;      
    Id(sigmayy,Ix,Iy+1,Iz) += w_trf*source_it;       
    Id(sigmayy,Ix+1,Iy+1,Iz) += w_trb*source_it;    
    Id(sigmayy,Ix,Iy,Iz+1) += w_blf*source_it;       
    Id(sigmayy,Ix+1,Iy,Iz+1) += w_blb*source_it;    
    Id(sigmayy,Ix,Iy+1,Iz+1) += w_brf*source_it;     
    Id(sigmayy,Ix+1,Iy+1,Iz+1) += w_brb*source_it;   

    Id(sigmazz,Ix,Iy,Iz) += w_tlf*source_it;         
    Id(sigmazz,Ix+1,Iy,Iz) += w_tlb*source_it;      
    Id(sigmazz,Ix,Iy+1,Iz) += w_trf*source_it;       
    Id(sigmazz,Ix+1,Iy+1,Iz) += w_trb*source_it;    
    Id(sigmazz,Ix,Iy,Iz+1) += w_blf*source_it;       
    Id(sigmazz,Ix+1,Iy,Iz+1) += w_blb*source_it;    
    Id(sigmazz,Ix,Iy+1,Iz+1) += w_brf*source_it;     
    Id(sigmazz,Ix+1,Iy+1,Iz+1) += w_brb*source_it;  
}


void velocidad_surface(float *sigmaxx,
                       float *sigmayy,
                       float *sigmazz,
                       float *sigmaxy,
                       float *sigmayz,
                       float *sigmazx, 
                       float *vx,
                       float *vy,
                       float *vz, 
                       float *bxs, 
                       float *bys, 
                       float *bzs, 
                       int *ind_s, 
                       int cont_s, 
                       float dt, 
                       float idx, 
                       float idy, 
                       float idz, 
                       int nx, 
                       int ny, 
                       int nz
                      )
{
    int i, j, k;

    #pragma omp parallel for private(i, j, k) 
    for (int ind = 0; ind < cont_s; ind++) {
        i = ind_s[ind] / (ny * nz);
        j = (ind_s[ind] - (i) * (ny * nz)) / nz;
        k = ind_s[ind] - ((i) * ny * nz + (j) * nz);

        Id(vx,i,j,k) += (dt) * bxs[ind] * (Dxf(sigmaxx,i,j,k,idx) + Dyb(sigmaxy,i,j,k,idy) + Dzb(sigmazx,i,j,k,idz));
        Id(vy,i,j,k) += (dt) * bys[ind] * (Dxb(sigmaxy,i,j,k,idx) + Dyf(sigmayy,i,j,k,idy) + Dzb(sigmayz,i,j,k,idz));
        Id(vz,i,j,k) += (dt) * bzs[ind] * (Dxb(sigmazx,i,j,k,idx) + Dyb(sigmayz,i,j,k,idy) + Dzf(sigmazz,i,j,k,idz));
    }
}

void velocidad_inside(float *rho,
                      float *sigmaxx,
                      float *sigmayy,
                      float *sigmazz,
                      float *sigmaxy,
                      float *sigmayz,
                      float *sigmazx, 
                      float *vx,
                      float *vy,
                      float *vz, 
                      int *ind_i, 
                      int cont_i, 
                      float dt, 
                      float idx, 
                      float idy, 
                      float idz, 
                      int nx, 
                      int ny, 
                      int nz
                      )
{
    int i, j, k;
    float b, b_i, b_j, b_k, bx, by, bz;

    #pragma omp parallel for private(i, j, k, b, b_i, b_j, b_k, bx, by, bz)
    for (int ind = 0; ind < cont_i; ind++) {
        i = ind_i[ind] / (ny * nz);
        j = (ind_i[ind] - (i) * (ny * nz)) / nz;
        k = ind_i[ind] - ((i) * ny * nz + (j) * nz);

        b  = 1 / Id(rho,i,j,k);
        b_i= 1 / Id(rho,i+1,j,k);
        b_j= 1 / Id(rho,i,j+1,k);
        b_k= 1 / Id(rho,i,j,k+1);

        bx = 2 * b * b_i / (b + b_i);
        by = 2 * b * b_j / (b + b_j);
        bz = 2 * b * b_k / (b + b_k);

        Id(vx,i,j,k) += (dt) * bx * (Dxf(sigmaxx,i,j,k,idx) + Dyb(sigmaxy,i,j,k,idy) + Dzb(sigmazx,i,j,k,idz));
        Id(vy,i,j,k) += (dt) * by * (Dxb(sigmaxy,i,j,k,idx) + Dyf(sigmayy,i,j,k,idy) + Dzb(sigmayz,i,j,k,idz));
        Id(vz,i,j,k) += (dt) * bz * (Dxb(sigmazx,i,j,k,idx) + Dyb(sigmayz,i,j,k,idy) + Dzf(sigmazz,i,j,k,idz));
    }
}

void abc(int nx, 
         int ny, 
         int nz, 
         int bound, 
         float *coef, 
         float *v)
{
    int i, j, k;

    #pragma omp parallel for collapse(3) private(i, j, k)
    for (i = 0; i < nx; i++) {
        for (j = 0; j < ny; j++) {
            for (k = 0; k < nz; k++) {

                if (i < bound) {
                    Id(v, i, j, k) *= coef[i];             //abc left side
                    Id(v, (nx-1)-i, j, k) *= coef[i];      //abc right side
                }
                if (j < bound) {
                    Id(v, i, j, k) *= coef[j];             //abc left side
                    Id(v, i, (ny-1)-j, k) *= coef[j];      //abc right side
                }
                if (k < bound) {
                    //Id(v, i, j) *= coef[j];              //abc top side
                    Id(v, i, j, (nz-1)-k) *= coef[k];      //abc bottom side
                }
            }
        }
    }
}

void gather(float *traceV,
            float *v,
            float *rece,
            int *top,
            int *ind_g,
            int cont_g,    
            float idx,
            float idy,
            float idz,
            int nx,
            int ny,
            int nz)
{
    int Gx, Gy, Gz;

    #pragma omp parallel for private(Gx, Gy, Gz)
    for (int ind = 0; ind < cont_g; ind++) {
        Gx = (int)(X(rece, ind_g[ind]) * idx);
        Gy = (int)(Y(rece, ind_g[ind]) * idy);
        Gz = (int)(Z(rece, ind_g[ind]) * idz) + top[Gx * ny + Gy];
        traceV[ind] = Id(v, Gx, Gy, Gz);
    }
}

void gatherVx(float *traceVx,
              float *vx,
              float *rece,
              float *vs,
              int *top,
              int *ind_g,
              int cont_g,    
              float idx,
              float idy,
              float idz,
              int nx,
              int ny,
              int nz)
{
    int i, j, k;
    float lx, ly, lz, dx, dy, dz;
    float w_tlf, w_tlb, w_trf, w_trb, w_blf, w_blb, w_brf, w_brb;
    float sum;
    float x0 = 1. / (2 * idx);

    #pragma omp parallel for private(i, j, k, lx, ly, lz, dx, dy, dz, w_tlf, w_tlb, w_trf, w_trb, w_blf, w_blb, w_brf, w_brb, sum)
    for (int ind = 0; ind < cont_g; ind++) {
		lx =  (X(rece,ind_g[ind])-x0)*idx;
	    ly =  (Y(rece,ind_g[ind]))*idy;
		i = (int)lx, j = (int)ly;
        lz =  (Z(rece,ind_g[ind]))*idz + top[(i)*ny+j];
		k = (int)lz;
		if ( (Id(vs,i+2,j,k)==0) || (Id(vs,i+2,j+1,k)==0)  ){
			lz =  (Z(rece,ind_g[ind]))*idz + top[(i+1)*ny+j];
			k = (int)lz;
		}

		dx = lx-i, dy = ly-j, dz = lz-k;

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		// w_tlf*= (Id(vs,i,j,k)>0);      
		// w_tlb*= (Id(vs,i+2,j,k)>0);   
		// w_trf*= (Id(vs,i,j+1,k)>0);    
		// w_trb*= (Id(vs,i+2,j+1,k)>0); 
		// w_blf*= (Id(vs,i,j,k+1)>0);    
		// w_blb*= (Id(vs,i+2,j,k+1)>0); 
		// w_brf*= (Id(vs,i,j+1,k+1)>0);  
		// w_brb*= (Id(vs,i+2,j+1,k+1)>0);

		sum = ( w_tlf + w_tlb + w_trf + w_trb + w_blf + w_blb + w_brf + w_brb );
		traceVx[ind] =  (1./sum)*(w_tlf*Id(vx,i,j,k)     +  w_tlb*Id(vx,i+1,j,k)    +
								  w_trf*Id(vx,i,j+1,k)   +  w_trb*Id(vx,i+1,j+1,k)  +
								  w_blf*Id(vx,i,j,k+1)   +  w_blb*Id(vx,i+1,j,k+1)  +
								  w_brf*Id(vx,i,j+1,k+1) +  w_brb*Id(vx,i+1,j+1,k+1));
	}
}



void gatherVy(float *traceVy,
              float *vy,
              float *rece,
              float *vs,
              int *top,
              int *ind_g,
              int cont_g,    
              float idx,
              float idy,
              float idz,
              int nx,
              int ny,
              int nz)
{
    int i, j, k;
    float lx, ly, lz, dx, dy, dz;
    float w_tlf, w_tlb, w_trf, w_trb, w_blf, w_blb, w_brf, w_brb;
    float sum;
    float y0 = 1. / (2 * idy);

    #pragma omp parallel for private(i, j, k, lx, ly, lz, dx, dy, dz, w_tlf, w_tlb, w_trf, w_trb, w_blf, w_blb, w_brf, w_brb, sum)
    for (int ind = 0; ind < cont_g; ind++) {
		lx =  (X(rece,ind_g[ind]))*idx;
	    ly =  (Y(rece,ind_g[ind])-y0)*idy;
		i = (int)lx, j = (int)ly;
        lz =  (Z(rece,ind_g[ind]))*idz + top[(i)*ny+j];
		k = (int)lz;
		if ( (Id(vs,i,j+2,k)==0) || (Id(vs,i+1,j+2,k)==0) ){
			lz =  (Z(rece,ind_g[ind]))*idz + top[(i)*ny+j+1];
			k = (int)lz;
		}

		dx = lx-i, dy = ly-j, dz = lz-k;

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		// w_tlf*= (Id(vs,i,j,k)>0);      
		// w_tlb*= (Id(vs,i+1,j,k)>0);   
		// w_trf*= (Id(vs,i,j+2,k)>0);    
		// w_trb*= (Id(vs,i+1,j+2,k)>0); 
		// w_blf*= (Id(vs,i,j,k+1)>0);    
		// w_blb*= (Id(vs,i+1,j,k+1)>0); 
		// w_brf*= (Id(vs,i,j+2,k+1)>0);  
		// w_brb*= (Id(vs,i+1,j+2,k+1)>0);

		sum = ( w_tlf + w_tlb + w_trf + w_trb + w_blf + w_blb + w_brf + w_brb );
		traceVy[ind] =  (1./sum)*(w_tlf*Id(vy,i,j,k)     +  w_tlb*Id(vy,i+1,j,k)    +
								  w_trf*Id(vy,i,j+1,k)   +  w_trb*Id(vy,i+1,j+1,k)  +
								  w_blf*Id(vy,i,j,k+1)   +  w_blb*Id(vy,i+1,j,k+1)  +
								  w_brf*Id(vy,i,j+1,k+1) +  w_brb*Id(vy,i+1,j+1,k+1));
	}
}

void gatherVz(float *traceVz,
              float *vz,
              float *rece,
              float *vs,
              int *top,
              int *ind_g,
              int cont_g,    
              float idx,
              float idy,
              float idz,
              int nx,
              int ny,
              int nz)
{
    int i, j, k;
    float lx, ly, lz, dx, dy, dz;
    float w_tlf, w_tlb, w_trf, w_trb, w_blf, w_blb, w_brf, w_brb;
    float sum;
    float z0 = 1. / (2 * idz);

    #pragma omp parallel for private(i, j, k, lx, ly, lz, dx, dy, dz, w_tlf, w_tlb, w_trf, w_trb, w_blf, w_blb, w_brf, w_brb, sum)
    for (int ind = 0; ind < cont_g; ind++) {
		lx =  (X(rece,ind_g[ind]))*idx;
	    ly =  (Y(rece,ind_g[ind]))*idy;
		i = (int)lx, j = (int)ly;
        lz =  (Z(rece,ind_g[ind])-z0)*idz + top[(i)*ny+j];
		k = (int)lz;
		dx = lx-i, dy = ly-j, dz = lz-k;

		w_tlf= (1-dz)*(1-dy)*(1-dx);   // top-left-front area
		w_tlb= (1-dz)*(1-dy)*dx;       // top-left-back area
		w_trf= (1-dz)*dy*(1-dx);       // top-right-front area
		w_trb= (1-dz)*dy*dx;           // top-right-back area
		w_blf= dz*(1-dy)*(1-dx);       // bottom-left-front area
		w_blb= dz*(1-dy)*dx;           // bottom-left-back area
		w_brf= dz*dy*(1-dx);           // bottom-right-front area
		w_brb= dz*dy*dx;               // bottom-right-back area

		// w_tlf*= (Id(vs,i,j,k)>0);      
		// w_tlb*= (Id(vs,i+1,j,k)>0);   
		// w_trf*= (Id(vs,i,j+1,k)>0);    
		// w_trb*= (Id(vs,i+1,j+1,k)>0); 
		// w_blf*= (Id(vs,i,j,k+1)>0);    
		// w_blb*= (Id(vs,i+1,j,k+1)>0); 
		// w_brf*= (Id(vs,i,j+1,k+1)>0);  
		// w_brb*= (Id(vs,i+1,j+1,k+1)>0);

		sum = ( w_tlf + w_tlb + w_trf + w_trb + w_blf + w_blb + w_brf + w_brb );
		traceVz[ind] =  (1./sum)*(w_tlf*Id(vz,i,j,k)     +  w_tlb*Id(vz,i+1,j,k)    +
								  w_trf*Id(vz,i,j+1,k)   +  w_trb*Id(vz,i+1,j+1,k)  +
								  w_blf*Id(vz,i,j,k+1)   +  w_blb*Id(vz,i+1,j,k+1)  +
								  w_brf*Id(vz,i,j+1,k+1) +  w_brb*Id(vz,i+1,j+1,k+1));
	}
}
