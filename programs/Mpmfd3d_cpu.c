/*
 ============================================================================
 Name        : pmfd3d_cpu.c
 Author      : Ivan Sánchez
 Version     :
 Copyright   :
 Description : Elastic Modeling including 3D Thread Layout, Sponge ABC.
 ============================================================================
 */


#include <rsf.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
//#include <sys/time.h>
#include<omp.h>
#include "functions_pmfd3d_cpu.c"

#define X(a,i) (a)[3*(i)]
#define Y(a,i) (a)[3*(i)+1]
#define Z(a,i) (a)[3*(i)+2]

int main(int argc, char* argv[]){
    double time_start, time_end;

   	int i,j;     // index variables
	int nt,nz,nx,ny,nz1,nx1,ny1; // dimensions
	int skip,skip_ts,ns,ng,nb;   // dimensions
	float dt,dz,dx,dy,idx,idy,idz,ox,oy,oz; // cells, & steptime sizes
	float Sxx,Szz,Syy,Vmax,Ts,Ts_field;    // source coordinates in meters

	// Initialize RSF 
	sf_init(argc,argv);
		
	// setup I/O files 
	sf_file Fw  = sf_input ("--input");  //wavelet input
	sf_file Fvp = sf_input ("vp");  //p-velocity media
	sf_file Fvs = sf_input ("vs");  //s-velocity media
	sf_file Frho = sf_input ("rho"); //rho-density media
	sf_file Fso = sf_input ("sou"); //sources
	sf_file Fre = sf_input ("rec"); //receivers
	sf_file Ffo = sf_output("--output"); //wave field [Vz,Vy,Vx]
	sf_file FgZ = sf_output("gatherZ");   // gather Z-component
	sf_file FgY = sf_output("gatherX");   // gather Y-component
	sf_file FgX = sf_output("gatherY");   // gather X-component
	sf_file Ftop = sf_output("top");  // Topography
	
	// parameter from the command line
	if (!sf_getfloat("Ts_field",&Ts_field))
		sf_error("Need Ts_field="); // skip for field animation

	if (!sf_getfloat("Ts",&Ts))
		sf_error("Need Ts=");   //  time sampling

	if (!sf_getfloat("Vmax",&Vmax))
		sf_error("Need Vmax="); // maximum velocity

	if (!sf_getint("nb",&nb)) nb = 20 ;

		
   // read time axis
	sf_axis axis_t  = sf_iaxa(Fw,1); //first value
		
	// read space axis	
	sf_axis axis_z = sf_iaxa(Fvp,1); //second value
	sf_axis axis_y = sf_iaxa(Fvp,2); //second value
	sf_axis axis_x = sf_iaxa(Fvp,3); //second value
	
	// Read sources and receivers axes parameters (n2,d2,o2)
	sf_axis axis_sou = sf_iaxa(Fso,2);
	sf_axis axis_rec = sf_iaxa(Fre,2);

	// read data information
	nt = sf_n(axis_t); //wavelet number data
    dt = sf_d(axis_t); //wavelet step size
	nz1 = sf_n(axis_z); //number dimension
    ny1 = sf_n(axis_y); //number dimension
    nx1 = sf_n(axis_x); //number dimension
	dz = sf_d(axis_z); //grid size dimension
	dy = sf_d(axis_y); //grid size dimension
	dx = sf_d(axis_x); //grid size dimension
	oz = sf_o(axis_z); //origin dimension
	oy = sf_o(axis_y); //origin dimension
	ox = sf_o(axis_x); //origin dimension
	ns = sf_n(axis_sou); //number of sources
	ng = sf_n(axis_rec); //number of receivers

	skip = (int) (Ts_field/dt);
	skip_ts=(int)(Ts/dt);
	int nsamples = (int)(nt/skip_ts)+1;

    // Write hypercube axes output
	sf_oaxa(Ffo,axis_z,1);
	sf_oaxa(Ffo,axis_y,2);
	sf_oaxa(Ffo,axis_x,3);
	sf_setn(axis_t,nt/skip); //Changes in axis length.
	sf_setd(axis_t,dt*skip); //Changes delta axis.
	sf_oaxa(Ffo,axis_t,4);   //Writes an axis.

	//Write gathers axes descriptions output
	//genera muestreo para trazas
	sf_axis axis_t2 = sf_iaxa(Fw,1); //first value

	sf_setn(axis_t2,nsamples); //Changes in axis length.
	sf_setd(axis_t2,dt*skip_ts); //Changes delta axis.

	sf_setd(axis_rec,1.0); //Changes delta axis.

	sf_oaxa(FgZ,axis_t2,1);   //(n1,o1,d1) Write the axis-1 (from input data)
	sf_oaxa(FgZ,axis_rec,2);  //Writes the axis-2, number of receivers
	sf_oaxa(FgZ,axis_sou,3);  //Writes the axis-3, number of sources

	sf_oaxa(FgY,axis_t2,1);   //(n1,o1,d1) Write the axis-1 (from input data)
	sf_oaxa(FgY,axis_rec,2);  //Writes the axis-2, number of receivers
	sf_oaxa(FgY,axis_sou,3);  //Writes the axis-3, number of sources

	sf_oaxa(FgX,axis_t2,1);   //(n1,o1,d1) Write the axis-1 (from input data)
	sf_oaxa(FgX,axis_rec,2);  //Writes the axis-2, number of receivers
	sf_oaxa(FgX,axis_sou,3);  //Writes the axis-3, number of sources

	//Write topography axes
	sf_oaxa(Ftop,axis_y,1);
	sf_oaxa(Ftop,axis_x,2);

	
	//read wavelet
  	float *wavelet=sf_floatalloc(nt);
  	sf_floatread(wavelet,nt,Fw);
	
   // read elastic model
	float *vp1=sf_floatalloc(nz1*nx1*ny1); 
    float *vs1=sf_floatalloc(nz1*nx1*ny1);
	float *rho1=sf_floatalloc(nz1*nx1*ny1); 
    sf_floatread(vp1,nz1*nx1*ny1,Fvp);
    sf_floatread(vs1,nz1*nx1*ny1,Fvs);
	sf_floatread(rho1,nz1*nx1*ny1,Frho);

	// expand the model
	ny = ny1 + 2*nb;
	nx = nx1 + 2*nb;
    nz = nz1 + nb;
    float *vp=sf_floatalloc(nz*nx*ny); 
    float *vs=sf_floatalloc(nz*nx*ny); 
    float *rho=sf_floatalloc(nz*nx*ny); 
    expand(vp,vp1,nz,nx,ny,nz1,nx1,ny1);
    expand(vs,vs1,nz,nx,ny,nz1,nx1,ny1);
    expand(rho,rho1,nz,nx,ny,nz1,nx1,ny1);

	//Array source receiver definition and reading
    float *sour=sf_floatalloc(3*ns); 
	sf_floatread(sour,3*ns,Fso);
	if (check_pos(sour,ns,ox,oy,oz,dx,dy,dz,nx1,ny1,nz1))
		sf_error("Sources position out of range");

    float *rece=sf_floatalloc(3*ng); 
	sf_floatread (rece,3*ng,Fre);
	if (check_pos(rece,ng,ox,oy,oz,dx,dy,dz,nx1,ny1,nz1))
		sf_error("Receivers position out of range");
	update_pos(rece,ng,nb,dx,dy);
      
	//Reciprocal of space sampling
	idx=1/dx; idy=1/dy; idz=1/dz;
   
    // Topography cdefinition
	int *top = sf_intalloc(nx*ny);
   	topography (vs, top, nx, ny, nz);
   
   // Definition surface and interior regions
	int cont_s, cont_i;
	indices_lengths(rho, nx, ny, nz, &cont_s, &cont_i);
    sf_warning("Surface points: %d\nInterior points: %d\n", cont_s, cont_i);
	int *ind_s = (int *)calloc(cont_s,sizeof(int));
	int *ind_i = (int *)calloc(cont_i,sizeof(int));
	indices_arrays(rho, nx, ny, nz, ind_s, ind_i);
	
	// --------- Receivers points -------------
	int cont_g=0;
	indices_length_rec(&cont_g, rece, ng, nx, idx);
	int *ind_g = sf_intalloc(cont_g);
	indices_array_rec(ind_g, rece, top, ng, nx, idx);
	
    // allocate fdtd arrays 
    float  *sigmaxx=sf_floatalloc(nz*nx*ny);
    float  *sigmayy=sf_floatalloc(nz*nx*ny);
    float  *sigmazz=sf_floatalloc(nz*nx*ny);
    float  *sigmaxy=sf_floatalloc(nz*nx*ny);
    float  *sigmayz=sf_floatalloc(nz*nx*ny);
    float  *sigmazx=sf_floatalloc(nz*nx*ny);
    float  *vx=sf_floatalloc(nz*nx*ny);
    float  *vy=sf_floatalloc(nz*nx*ny);
    float  *vz=sf_floatalloc(nz*nx*ny);


	// --------- CAMPOS EN CPU -------------
	float *traceVx = sf_floatalloc(nsamples*ng);
   	float *traceVy = sf_floatalloc(nsamples*ng);
   	float *traceVz = sf_floatalloc(nsamples*ng); 
	float *traceVx0 = sf_floatalloc(cont_g); 
   	float *traceVy0 = sf_floatalloc(cont_g);
	float *traceVz0 = sf_floatalloc(cont_g);
  
   	// --------- PARAMETROS EN LA SUPERFICIE EN CPU --------------------------------------------------------------------
  	float *bxs = sf_floatalloc(cont_s);
	float *bys = sf_floatalloc(cont_s);
	float *bzs = sf_floatalloc(cont_s);
	float *mu_xys = sf_floatalloc(cont_s);
	float *mu_yzs = sf_floatalloc(cont_s);
	float *mu_zxs = sf_floatalloc(cont_s);
	float *n1_xxs = sf_floatalloc(cont_s);
	float *n2_xxs = sf_floatalloc(cont_s);
	float *n3_xxs = sf_floatalloc(cont_s);
	float *n1_yys = sf_floatalloc(cont_s);
	float *n2_yys = sf_floatalloc(cont_s);
	float *n3_yys = sf_floatalloc(cont_s);
	float *n1_zzs = sf_floatalloc(cont_s);
	float *n2_zzs = sf_floatalloc(cont_s);
	float *n3_zzs = sf_floatalloc(cont_s);

	// --------- CALCULO DE PARAMETROS EN LA SUPERFICIE EN CPU -------------------------------------------------------//
	parameter_fs(vp,      vs,      rho,
				 bxs,     bys,     bzs,
				 mu_xys,  mu_yzs,  mu_zxs,
				 n1_xxs,  n2_xxs,  n3_xxs,
				 n1_yys,  n2_yys,  n3_yys,
				 n1_zzs,  n2_zzs,  n3_zzs,
				 ind_s,	  cont_s,
				 nx,	  ny, nz);

    // ABC
	float *coef = (float *)calloc(nb,sizeof(float));
    float R = 1e-3;
    init_abc(nb,Vmax,R,dt,dz,coef);

	// ----------- PROPAGATION ---------------------------------------------
	time_start = time_measure();

	//begin source loop //

   	for (int shot=0; shot<ns; shot++){


		//--------- PRINT SOURCES LOCATIONS AND DIMENSION MODEL ---------------------------------------------------------------------------//
		Sxx = X(sour,shot)+nb*dx; 
		Syy = Y(sour,shot)+nb*dy; 
		Szz = Z(sour,shot);

		sf_warning("\n===== Model dimension ==============\n");
		sf_warning("nz=%d \t\tny=%d \t\tnx=%d  \n", nz, ny, nx);
		
		sf_warning("\n===== Source location ==============\n");
		sf_warning("Shot %d of %d\n", shot+1, ns);
		sf_warning("Sou_x=%.3f \t\tSou_y=%.3f \t\tSou_z=%.3f  \n", X(sour,shot), Y(sour,shot), Szz);

        memset(traceVx, 0, sizeof(float)*nsamples*ng );
		memset(traceVy, 0, sizeof(float)*nsamples*ng );
		memset(traceVz, 0, sizeof(float)*nsamples*ng );

		//begin time loop //

		for ( int it = 0; it < nt; ++it) {	//****************************************
        
        sigma_surface(vx, vy, vz, sigmaxx, sigmayy, sigmazz, sigmaxy, sigmayz, sigmazx, mu_xys, mu_yzs, mu_zxs, n1_xxs, n2_xxs, n3_xxs, n1_yys, n2_yys, n3_yys, n1_zzs, n2_zzs, n3_zzs, ind_s, cont_s, dt, idx, idy, idz, nx, ny, nz);

        sigma_inside(vp, vs, rho, vx, vy, vz, sigmaxx, sigmayy, sigmazz, sigmaxy, sigmayz, sigmazx, ind_i, cont_i, dt, idx, idy, idz, nx, ny, nz);
        
        source_insert(wavelet[it], sigmaxx, sigmayy, sigmazz, top, idx, idy, idz, nx, ny, nz, Sxx, Syy, Szz);

        velocidad_surface(sigmaxx, sigmayy, sigmazz, sigmaxy, sigmayz, sigmazx, vx, vy, vz, bxs, bys, bzs, ind_s, cont_s, dt, idx, idy, idz, nx, ny, nz);

        velocidad_inside(rho, sigmaxx, sigmayy, sigmazz, sigmaxy, sigmayz, sigmazx, vx, vy, vz, ind_i, cont_i, dt, idx, idy, idz, nx, ny, nz);

        abc(nx, ny, nz, nb, coef, vx);
        abc(nx, ny, nz, nb, coef, vy);
        abc(nx, ny, nz, nb, coef, vz);
        abc(nx, ny, nz, nb, coef, sigmaxx);
        abc(nx, ny, nz, nb, coef, sigmayy);
        abc(nx, ny, nz, nb, coef, sigmazz);
        abc(nx, ny, nz, nb, coef, sigmaxy);
        abc(nx, ny, nz, nb, coef, sigmayz);
        abc(nx, ny, nz, nb, coef, sigmazx);

        // Geophone gather 
		if(it% skip_ts==0){
            gatherVx(traceVx0, vx, rece, vs, top, ind_g, cont_g, idx, idy, idz, nx, ny, nz);
            gatherVy(traceVy0, vy, rece, vs, top, ind_g, cont_g, idx, idy, idz, nx, ny, nz);
            gatherVz(traceVz0, vz, rece, vs, top, ind_g, cont_g, idx, idy, idz, nx, ny, nz);

		    for (int g=0; g< cont_g; g++){
				traceVx[ind_g[g]*nsamples+(it/skip_ts-1)]=-traceVx0[g]; //Change sign x-component
				traceVy[ind_g[g]*nsamples+(it/skip_ts-1)]=-traceVy0[g]; //Change sign y-component
				traceVz[ind_g[g]*nsamples+(it/skip_ts-1)]=traceVz0[g];
			}

        }

        // write wavefield to output
        if(it%skip==0){
            window(vp1,vz,nz1,nx1,ny1,nz,nx,ny);
            sf_floatwrite(vp1,nz1*nx1*ny1,Ffo);
        }
        sf_warning("%d%s;",it*100/nt,"%");

        } // end time loop


		//sf_floatwrite(field_vz,nx*ny*nz*(nt/skip),Ffo);	
		sf_floatwrite(traceVx,nsamples*ng,FgX);	
		sf_floatwrite(traceVy,nsamples*ng,FgY);	
		sf_floatwrite(traceVz,nsamples*ng,FgZ);	

    } // end source loop

	int topmax = top_max (vs, nx, ny, nz);
    float *top2 = (float *)calloc(nx1*ny1,sizeof(float));
    for ( i = 0; i < nx1; i++){
		for ( j = 0; j < ny1; j++){
			top2[i*ny1+j] = dz*(topmax-top[(i+nb)*ny+(j+nb)]);     //Topography in meters
		}
	} 
		
    sf_floatwrite( top2, nx1*ny1, Ftop );

	time_end = time_measure();
	sf_warning("Elapsed time is: %f\n", (time_end - time_start));

	sf_close();
    /* deallocate arrays */
    free(sigmaxx); free(sigmayy); free(sigmazz);  
    free(sigmaxy); free(sigmayz); free(sigmazx);
    free(vx);       free(vy);       free(vz);
    free(n1_xxs);    free(n2_xxs);    free(n3_xxs);
    free(n1_yys);    free(n2_yys);    free(n3_yys);
    free(n1_zzs);    free(n2_zzs);    free(n3_zzs);
    free(bxs);       free(bys);       free(bzs);
    free(mu_xys);    free(mu_yzs);    free(mu_zxs);
    free(vp);     free(vs);     free(rho);      
    free(vp1);     free(vs1);     free(rho1); 
    free(wavelet); 
    free(traceVx0);    free(traceVy0);     free(traceVz0);
    free(traceVx);    free(traceVy);     free(traceVz);
	free(ind_i);        free(ind_s);      free(ind_g);
  	free(coef);       free(top); 			 free(top2);
	free(rece);

		
   return(0);
}
