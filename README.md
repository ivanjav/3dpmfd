# ElasWave3D

## Getting started

ElasWave3D, a novel 3D solver based on the finite-difference method, tailored for elastic wave modeling in complex geological areas with irregular topography. This solver utilizes a unique unstructured index array representation to implement the parameter modified formulation, ensuring the accommodation of topographic variations. Validated against the well-established SPECFEM3D solver, ElasWave3D demonstrated accuracy with misfit errors of less than 1\% in most cases. Notably, it achieves a computational speed-up approximately 20 times faster than the CPU implementation, enabling cost-effective and detailed simulations of near-surface seismic wave propagation in heterogeneous earth models with varying topographical features.

## Description

Due to the limited availability of open-source software capable of simulating three-dimensional elastic wave propagation in heterogeneous media with irregular topography, we developed a solver named ElasWave3D. Based on the FDM and specifically designed for GPU acceleration, this solver adopts an irregular subdomain index array strategy to manage memory and grid accesses in irregularly shaped regions more efficiently. In addition, it integrates the parameter-modified (PM) formulation introduced by Cao and Chen (2018), which enforces accurate free-surface conditions on rectangular grids even in the presence of complex topography. The PM formulation has been shown to achieve accuracy levels comparable to those of SPECFEM3D when sampling 15 grid points per minimum wavelength (Cao and Chen, 2018). In our comparisons with SPECFEM3D, ElasWave3D achieved misfit errors close to 1\% in a homogeneous model with rough topography. Moreover, compared to the CPU implementation, this solver increases speed-up approximately 20 times faster by leveraging GPU acceleration. 

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ivanjav/pmfd3d.git
git branch -M main
git push -uf origin main
```

## Installation and Run 

**cd ~/ElasWave3D/programs/** \
**scons**

**cd ../examples/homo_3d_topo/** \
**scons** \
**scons view** 

## Note

Requires an NVIDIA-gpu with a RAM larger than 6GB. It also requires Madagascar (open source software) for multidimensional data analysis and reproducible computational experiments.

https://www.reproducibility.org/wiki/Main_Page


## Authors and acknowledgment

Ivan Javier Sánchez-Galvis
jsangal@correo.uis.edu.co
ivan.sanchez@ucalgary.ca

Herling Gonzalez-Alvarez
herling.gonzalez@ecopetrol.com.co

This research was conducted within the framework of the Agreement “Acta No. 27 del Convenio de Cooperación Tecnológica 5222395” between Universidad Industrial de Santander and Ecopetrol S.A.– Centro de Innovación y Tecnología ICP. Furthermore, this work was funded by CREWES industrial sponsors and the Natural Science and Engineering Research Council of Canada (NSERC) through the grant CRDPJ 543578-19, with additional support provided by Emissions Reduction Alberta through the ACT4-SPARSE project.

## License
This work is licensed under the GNU General Public License (GPL) v3.0. See the
LICENSE file for more details.

